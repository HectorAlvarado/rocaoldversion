'use strict';

var noCache = require('connect-nocache')();
var express = require('express');
var router = express.Router();
var user = require('../controllers/users');
var notification = require('../controllers/notifications');
var client = require('../controllers/clients');
var asset = require('../controllers/assets');
var humanResource = require('../controllers/humanResources');
var auth = require('../config/auth');
var role = require('../controllers/roles');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

/* Module Assets*/
router.get('/asset/getAllAssets', asset.getAllAssets);
router.post('/asset/addAsset', multipartMiddleware, asset.addAsset);
router.post('/asset/editAsset', multipartMiddleware, noCache, asset.editAsset);
router.post('/asset/rentAsset', multipartMiddleware, asset.rentAsset);
router.post('/asset/finishRentAsset', multipartMiddleware, asset.finishRentAsset);
router.post('/asset/sellAsset', multipartMiddleware, asset.sellAsset);
router.post('/asset/leaseAsset', multipartMiddleware, asset.leaseAsset);

/* Module Human Resources */
router.get('/humanResource/getAllHumanResources', auth.isAuthenticated, humanResource.getAllHumanResources);
router.post('/humanResource/addHumanResource', auth.isAuthenticated, multipartMiddleware, humanResource.addHumanResource);
router.post('/humanResource/editHumanResource', auth.isAuthenticated, noCache, multipartMiddleware, humanResource.editHumanResource);
router.post('/humanResource/deleteHumanResource', auth.isAuthenticated, humanResource.deleteHumanResource);

/* Module Clients */
router.post('/clients/getAllClients', auth.isAuthenticated, client.getAllClients);
router.post('/clients/addClient', auth.isAuthenticated, client.addClient);
router.post('/clients/editClient', auth.isAuthenticated, client.editClient);
router.post('/clients/eliminateClient', auth.isAuthenticated, client.eliminateClient);

/* User's module */
router.post('/users/createUser', user.createUser);
router.post('/users/getAllUsers', auth.isAuthenticated, user.getAllUsers);
router.post('/users/getAllhumanResources', auth.isAuthenticated, user.getAllhumanResources);
router.post('/users/deleteUser', auth.isAuthenticated, user.deleteUser);
router.post('/users/editUser', auth.isAuthenticated, user.updateUser);

/* Login's module */
router.post('/login', auth.authenticate);
router.get('/logout', auth.userLogout);
router.get('/status', auth.userStatus);

/* Notifictaions module */
router.post('/addNotification', notification.createNotification);
router.post('/updateNotifications', auth.isAuthenticated, notification.updateNotifications);
router.post('/deleteNotifications', auth.isAuthenticated, notification.deleteNotifications);

//Si lleva imagenes usar asi con el multipartyMiddleware si no no jala :(
//router.post('/otro', multipartyMiddleware, user.otro);

/* GET home page. */
router.get('*', function(req, res) {
    res.render('index', {
        bootstrappedUser: req.user
    });
});

module.exports = router;