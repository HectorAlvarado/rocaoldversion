'use strict';

var express = require('express');
var app = express();

var config = {
    rootPath: __dirname
};

// All documentation has been mood to proper folders in config at the root
require('./config/express')(app, config);

// Models for the mongoose Database
require('./models/user');
require('./models/asset');
require('./models/asset_deletd');
require('./models/asset_history');
require('./models/asset_received');
require('./models/assigment');
require('./models/client');
require('./models/department');
require('./models/engine_asset');
require('./models/human_resource');
require('./models/human_resources_history');
require('./models/local_assigment');
require('./models/location');
require('./models/notification');
require('./models/rent');
require('./models/sale');
require('./models/lease');
require('./models/role');
require('./models/user');
require('./models/user_working_day');


var routes = require('./routes/index');
//var users = require('./routes/users');

// Routes
//require('./config/routes')(app, routes, users);
require('./config/routes')(app, routes);

// Mongoose
require('./config/mongoose')(app);

// Passport for login
require('./config/passport')();

module.exports = app;