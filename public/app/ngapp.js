'use strict';
var app = angular.module('ROCA', ['ngRoute', 'ngFileUpload', 'ngImgCrop']);

app.config(['$routeProvider', function($routeProvider) {

    $routeProvider.
    when('/inicio_sesion', {
        templateUrl: 'partials/views/authentication/login',
        controller: 'rcLoginCtrl',
        access: { restricted: false }
    }).
    when('/inicio', {
        templateUrl: 'partials/views/main/home',
        controller: 'rcHomeCtrl',
        access: { restricted: true }
    }).
    when('/activo_fijo', {
        templateUrl: 'partials/views/assets/assets',
        controller: 'rcAssetsCtrl',
        access: { restricted: true }
    }).
    when('/Ubicaciones', {
        templateUrl: 'partials/views/locations/locations',
        controller: 'rcLocationsCtrl',
        access: { restricted: true }
    }).
    when('/clientes', {
        templateUrl: 'partials/views/clients/clients',
        controller: 'rcClientsCtrl',
        access: { restricted: true }
    }).
    when('/recursos_humanos', {
        templateUrl: 'partials/views/humanResource/humanResource',
        controller: 'rcHumanResourceCtrl',
        access: { restricted: true }
    }).
    when('/usuarios', {
        templateUrl: 'partials/views/users/users',
        controller: 'rcUsersCtrl',
        access: { restricted: true }
    }).
    otherwise({
        redirectTo: '/inicio_sesion',
        access: { restricted: false }
    });
}]);

app.run(function($rootScope, $location, $route, rcUserInfoFcty) {
    $rootScope.$on('$routeChangeStart',
        function(event, next, current) {
            rcUserInfoFcty.getUserStatus()
                .then(function() {
                    if (next.access.restricted && !rcUserInfoFcty.isLoggedIn()) {
                        $location.path('/');
                        $route.reload();
                    }
                });
        });
});

function go(url) {
    window.location = url;
}

function goInNewTab(url) {
    window.open(url);
}

function goBack() {
    window.history.back();
}