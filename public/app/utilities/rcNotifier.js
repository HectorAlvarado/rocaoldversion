'use strict';

app.value('rcToastr', toastr);

app.factory('rcNotifier', function(rcToastr){
	return {
		success: function(msg){
			rcToastr.success(msg);
		},
		error: function(msg){
			rcToastr.error(msg);
		},
		notify: function(msg){
			rcToastr.info(msg);
		}
	};
});
