'use strict';

var timeAnim;
var tiemRequest;
var is_loading = false;

function load(isLoading) {
    if (isLoading) loading();
    else loadReady();
}

function loadReady() {
    if (!is_loading) return;
    var section = document.getElementById('loadSection');
    try { document.body.removeChild(section); } catch (e) {}
    clearInterval(timeAnim);
    is_loading = false;
}

function setLoadingStyle(ls, lt) {
    ls.backgroundColor = 'rgba(0,0,0,0.8)';
    ls.position = lt.position = 'absolute';
    ls.top = ls.bottom = ls.left = ls.right = '0px';
    ls.color = 'white';
    ls.fontFamily = '"Segoe UI", Tahoma, Geneva, Verdana, sans-serif';
    ls.zIndex = "9998";
    lt.width = '144px';
    lt.height = '40px';
    lt.top = lt.left = '50%';
    lt.margin = '-20px 0px 0px -72px';
    lt.fontSize = '24px';
    lt.zIndex = "9999";
}

function loading() {
    var loadSection = document.createElement('div'),
        loadTxt = document.createElement('div');
    is_loading = true;

    loadSection.setAttribute('id', 'loadSection');
    loadTxt.setAttribute('id', 'loadTxt');
    loadTxt.innerHTML = 'CARGANDO';

    setLoadingStyle(loadSection.style, loadTxt.style);

    loadSection.appendChild(loadTxt);
    document.body.appendChild(loadSection);

    timeSetting();
}

function timeSetting() {
    timeAnim = setInterval(function() {
        var loadTxt = document.getElementById('loadTxt');
        loadTxt.innerHTML = loadTxt.innerHTML.length === 11 ? 'CARGANDO' : loadTxt.innerHTML + '.';
    }, 200);
}