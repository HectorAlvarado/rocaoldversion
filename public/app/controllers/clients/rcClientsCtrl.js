'use strict';

app.controller('rcClientsCtrl', function($scope, $http, rcUserInfoFcty, rcClientsFcty, rcNotifier, rcHelpersFcty, rcCommonSrc) {

    rcUserInfoFcty.setCurrentModule(3);
    $scope.userInfo = rcUserInfoFcty;
    $scope.clients = rcClientsFcty;
    $scope.regex = rcHelpersFcty.regex;
    $scope.sub_menu_sel = 1;
    $scope.open_dialog = false;
    $scope.editable_client = true;
    $scope.dialog_opened = 0;
    $scope.current_client = {};
    $scope.previ3ous_client = {};
    $scope.previous_client_index = -1;
    $scope.client_was_edited = false;
    $scope.states = rcCommonSrc.states;
    $scope.open_delete_client_dialog = false;

    $scope.Initialize = function() {
        $scope.initializeModels();
        $scope.getAllClients();
    };

    $scope.selectOption = function(option) {
        $scope.sub_menu_sel = option;
    };

    $scope.openDialog = function(id_dialog) {
        $scope.open_dialog = true;
        $scope.dialog_opened = id_dialog;
    };

    $scope.openNewClientDialog = function() {
        $scope.initializeModels();
        $scope.editable_client = true;
        $scope.openDialog(1);
    };

    $scope.openClientDetailDialog = function(client) {
        var address = rcHelpersFcty.addres.getSeparateAddress(client.address);
        $scope.current_client = client;
        $scope.current_client.address_parts = {};
        $scope.current_client.address_parts.street = address[0];
        $scope.current_client.address_parts.suburb = address[1];
        $scope.current_client.address_parts.state = address[2];
        $scope.editable_client = false;
        $scope.openDialog(2);
    };

    $scope.openEditableClientDialog = function(client) {
        $scope.closeDialog();
        $scope.previous_client_index = rcClientsFcty.indexOf(client);
        $scope.previous_client = rcHelpersFcty.cloner.clone(client);
        $scope.current_client = client;
        $scope.editable_client = true;
        $scope.openDialog(3);
    };

    $scope.openDeleteClientDialog = function() {
        $scope.open_delete_client_dialog = true;
    }

    $scope.closeDialog = function() {
        if ($scope.dialog_opened == 3 && !$scope.client_was_edited)
            rcClientsFcty.items[$scope.previous_client_index] = $scope.previous_client;

        $scope.open_dialog = false;
        $scope.client_was_edited = false;
        $scope.dialog_opened = 0;
        $scope.initializeModels();
    };

    $scope.closeDeleteClientDialog = function() {
        $scope.open_delete_client_dialog = false;
    };

    $scope.getAllClients = function() {
        rcClientsFcty.getAllClients();
        log($scope.clients);
    };

    $scope.addClient = function(client) {
        console.log('from controller');
        console.log(client);
        $scope.isClientFormInvalid(
            // On Succes
            function() {
                load(true);
                rcClientsFcty.add(client).then(function(success) {
                    load(false);
                    if (success) {
                        rcNotifier.success('Cliente agregado exitosamente.');
                        $scope.closeDialog();
                    } else {
                        rcNotifier.error('Error, inténtelo más tarde.');
                        $scope.closeDialog();
                    }
                });
            },
            // On Error
            function(err) {
                rcNotifier.error(err);
            }
        );
    };

    $scope.editClient = function(client) {
        $scope.isClientFormInvalid(
            // On Succes
            function() {
                load(true);
                var indx = rcClientsFcty.indexOf(client);
                rcClientsFcty.update(client, indx).then(function(success) {
                    load(false);
                    if (success) {
                        $scope.client_was_edited = true;
                        rcNotifier.success('Cliente editado exitosamente.');
                        $scope.closeDialog();
                    } else {
                        rcNotifier.error('Error, inténtelo más tarde.');
                        $scope.closeDialog();
                    }
                });
            },
            // On Error
            function(err) {
                rcNotifier.error(err);
            }
        );
    };

    $scope.deleteClient = function(client) {
        load(true);
        rcClientsFcty.delete(client).then(function(success) {
            load(false);
            if (success) {
                rcNotifier.success('Cliente eliminado exitosamente.');
                $scope.closeDeleteClientDialog();
                $scope.closeDialog();
            } else {
                rcNotifier.êrror('Error, inténtelo más tarde');
            }
        });
    };

    $scope.isClientFormInvalid = function(onSucces, onError) {
        var error = '';

        //General
        if ($scope.client_form.fiscal_name.$invalid)
            error += '- Nombre fiscal es campo requerido.<br>';
        if ($scope.client_form.name.$invalid)
            error += '- Nombre es campo requerido.<br>';
        if ($scope.client_form.rfc.$invalid)
            error += '- RFC es campo requerido.<br>';
        if ($scope.client_form.zip.$invalid)
            error += '- Ingrese un código postal válido.<br>';

        //Address
        if ($scope.client_form.street.$invalid ||
            $scope.client_form.suburb.$invalid ||
            $scope.client_form.state.$invalid)
            error += '- Dirección es campo requerido.<br>';
        else if ($scope.client_form.suburb.$modelValue.includes(","))
            error += '- Colonia no puede incluir comas (,)<br>';

        //Client 1
        if ($scope.client_form.contact_name_1.$invalid ||
            $scope.client_form.contact_mail_1.$invalid ||
            $scope.client_form.contact_phone_1.$invalid)
            error += '- Contacto 1 es requerido, verifique los datos.<br>';

        //Client 2
        if ($scope.client_form.contact_name_2.$invalid ||
            $scope.client_form.contact_mail_2.$invalid ||
            $scope.client_form.contact_phone_2.$invalid)
            error += '- Verifique los datos de contacto 2.<br>';

        //Client 3
        if ($scope.client_form.contact_name_3.$invalid ||
            $scope.client_form.contact_mail_3.$invalid ||
            $scope.client_form.contact_phone_3.$invalid)
            error += '- Verifique los datos de contacto 3.<br>';

        if ($scope.client_form.$invalid)
            onError(error);
        else
            onSucces();
    };

    $scope.initializeModels = function() {
        $scope.current_client = {
            "name": "",
            "rfc": "",
            "zip": "",
            "category": "",
            "created_date": "",
            "fiscal_name": "",
            "address": "",
            "address_parts": {
                "street": "",
                "suburb": "",
                "state": ""
            },
            "contact_name_1": "",
            "contact_mail_1": "",
            "contact_phone_1": "",
            "contact_mail_2": "",
            "contact_name_2": "",
            "contact_phone_2": "",
            "contact_name_3": "",
            "contact_mail_3": "",
            "contact_phone_3": ""
        };
    };
});