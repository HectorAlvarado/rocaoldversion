'use strict';

app.factory('rcClientsFcty', function($http, $q, $location, $route) {
    var services = {};

    services.items = [];

    services.getAllClients = function() {
        var obj = {};
        obj.permissions = ['Clients']; // [0] -> MODULO, [1] -> SUBMODULO, [2] -> TAREA
        $http.post('/clients/getAllClients', obj).then(function(response) {
            console.log('RESPUESTA DE PERMISSIONS');
            console.log(response.data);
            if (!response.data) {
                $location.path('/');
                $route.reload();
            } else {
                angular.copy(response.data, services.items);
            }
        });
    };

    services.getAllClientsForRentSale = function() {
        var items = [{
                "id": "1a234feefad234",
                "name": "Plomería selecta",
                "category": "Plomería"
            },
            {
                "id": "1fad234a234fee",
                "name": "Aluvipesa",
                "category": "Herrería"
            },
            {
                "id": "1eddcaaefad234",
                "name": "Margonz",
                "category": "Autorefacciones"
            },
            {
                "id": "efad2341eddcaa",
                "name": "Aceros Tangamanga",
                "category": "Herrería"
            },
        ];

        return items;
    }

    services.add = function(client) {
        var dfd = $q.defer();

        client.address = getAddress(client.address_parts);
        var obj = {};
        obj.newClient = client;
        //obj.permissions = {modulo1, sub1, task1}
        $http.post('/clients/addClient', obj).then(function(response) {
            if (response.data) {
                var addressArray = getSeparateAddress(response.data.address);
                response.data.address_parts = {};
                response.data.address_parts.street = addressArray[0];
                response.data.address_parts.suburb = addressArray[1];
                response.data.address_parts.state = addressArray[2];
                services.items.push(response.data);
                dfd.resolve(true);
            } else {
                console.log('error in factory after server');
                dfd.resolve(false);
            }
        }, function(response) {
            console.log('Error status: ' + response.status);
        });

        return dfd.promise;
    };

    services.delete = function(client) {

        var obj = {};
        obj.clientToEliminate = client;
        var dfd = $q.defer();

        $http.post('/clients/eliminateClient', obj).then(function(response) {
            if (response) {
                var index;
                index = services.items.indexOf(client);
                services.items.splice(index, 1);
                dfd.resolve(true);
            } else {
                dfd.resolve(false);
            }
        });
        return dfd.promise;
    };

    services.update = function(client, index) {

        var obj = {};
        var dfd = $q.defer();

        client.address = getAddress(client.address_parts);

        obj.editClient = client;
        $http.post('/clients/editClient', obj).then(function(response) {
            if (response) {
                console.log(response);
                services.items[index] = response.data;
                dfd.resolve(true);
            } else {
                dfd.resolve(false);
            }
        });
        return dfd.promise;
        /*
        client.address = getAddress(client.address_parts);
        services.items[index] = client;
        return services;
        */
    };

    services.indexOf = function(client) {
        return services.items.indexOf(client);
    };

    function getAddress(address_parts) {
        address_parts.street = address_parts.street.replace(",", " ");
        address_parts.suburb = address_parts.suburb.replace(",", " ");
        return address_parts.street + ', ' +
            address_parts.suburb + ', ' +
            address_parts.state;
    }

    function getSeparateAddress(address) {
        var separate_address = address.split(',');

        if (separate_address.length && separate_address.length == 3) {
            separate_address[0] = separate_address[0].trim();
            separate_address[1] = separate_address[1].trim();
            separate_address[2] = separate_address[2].trim();
            return separate_address;
        } else
            return ['Calle', 'Colonia', 'Estado'];
    }

    return services;
});