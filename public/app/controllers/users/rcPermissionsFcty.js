'use strict';

app.factory('rcPermissionsFcty', function() {
    var services = {};

    services.items = [{
            "name": "Residente",
            "permission": '{"name":"Residente","permissions":{"human_resource":true}}'
        },
        {
            "name": "Administrador",
            "permission": '{"name":"Administrador","permissions":true}'
        },
        {
            "name": "Recursos Humanos",
            "permission": '{"name":"Recursos Humanos","permissions":{"human_resource":true,"clients":true}}'
        }
    ];

    services.roles = [];

    services.modules = [
        "Activo Fijo",
        "Ubicaciones",
        "Clientes",
        "R. Humanos",
        "Usuarios"
    ];

    services.getAllPermissions = function() {

        // get all name permissions (not remove)
        services.getAllPermissionsNames();
    };

    services.getAllPermissionsNames = function() {
        for (var i in services.items)
            services.roles.push(services.items[i].name);
    }

    services.add = function(permission) {
        services.items.push(permission);
        return services;
    };

    services.delete = function(permission) {
        var index;
        index = services.items.indexOf(permission);
        services.items.splice(index, 1);
        return services;
    };

    services.update = function(permission, index) {
        services.items[index] = permission;
        return services;
    };

    services.indexOf = function(permission) {
        return services.items.indexOf(permission);
    };

    services.getPermissionByName = function(permission_name) {
        var permission = "_";

        for (var i = 0; i < services.items.length; i++) {
            if (services.items[i].name == permission_name) {
                permission = services.items[i].permission;
                break;
            }
        }

        return permission;
    }

    return services;
});