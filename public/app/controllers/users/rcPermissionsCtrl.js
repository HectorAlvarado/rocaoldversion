'use strict';

app.controller('rcPermissionsCtrl', function($scope, $http, rcUserInfoFcty, rcPermissionsFcty, rcNotifier) {

    rcUserInfoFcty.setCurrentModule(5); //*
    $scope.userInfo = rcUserInfoFcty;
    $scope.sub_menu_sel = 1;
    $scope.users = rcUsersFcty; //*
    $scope.open_dialog = false;
    $scope.dialog_opened = 0;
    $scope.current_user = {}; //*
    $scope.editable_user = true;
    $scope.previous_user = {};
    $scope.previous_user_index = -1;
    $scope.user_was_edited = false;
    $scope.open_delete_user_dialog = false;

    $scope.Initialize = function() {
        $scope.initializeModels();
    };

    $scope.selectOption = function(option) {
        $scope.sub_menu_sel = option;
    };

    $scope.openDialog = function(id_dialog) {
        $scope.open_dialog = true;
        $scope.dialog_opened = id_dialog;
    };

    $scope.openNewUserDialog = function() {
        $scope.initializeModels();
        $scope.editable_user = true;
        $scope.openDialog(1);
    };

    $scope.openUserDetailDialog = function(user) {
        $scope.current_user = user;
        $scope.editable_user = false;
        $scope.openDialog(2);
    };

    $scope.openEditableUseDialog = function(user) {
        $scope.closeDialog();
        $scope.previous_user_index = rcUsersFcty.indexOf(user);
        $scope.previous_user = rcHelpersFcty.cloner.clone(user);
        $scope.current_user = user;
        $scope.editable_user = true;
        $scope.openDialog(3);
    };

    $scope.openDeleteUserDialog = function() {
        $scope.open_delete_user_dialog = true;
    }

    $scope.closeDialog = function() {
        if ($scope.dialog_opened == 3 && !$scope.user_was_edited)
            rcUsersFcty.items[$scope.previous_user_index] = $scope.previous_user;

        $scope.open_dialog = false;
        $scope.user_was_edited = false;
        $scope.dialog_opened = 0;
        $scope.initializeModels();
    };

    $scope.closeDeleteClientDialog = function() {
        $scope.open_delete_user_dialog = false;
    };
    
    $scope.getAllUsers = function() {
        
    };

    $scope.addUser = function(user) {
        /* Delete after deployment */
        $scope.isUsersFormInvalid(
            // On Succes
            function() { rcUsersFcty.add(user); },
            // On Error
            function(err) { rcNotifier.error(err); }
        );
        /* Uncomment after deployment */
        /*load(true);
        $scope.isUsersFormInvalid(
            // On Succes
            function() {
                rcUsersFcty.add(user).then(function(success) {
                    load(false);
                    if (success) {
                        rcNotifier.success('Usuario agregado exitosamente');
                        $scope.closeDialog();
                    } else {
                        rcNotifier.error('Error, intentelo más tarde');
                        $scope.closeDialog();
                    }
                });
            },
            // On Error
            function(err) {
                rcNotifier.error(err);
            }
        );*/
    };

    $scope.editUser = function(user) {
        /* Delete after deployment */
        $scope.isUsersFormInvalid(
            // On Succes
            function() {
                var indx = rcUsersFcty.indexOf(user);
                rcUsersFcty.update(user, indx);
            },
            // On Error
            function(err) { rcNotifier.error(err); }
        );
        /* Uncomment after deployment */
        /*load(true);
        $scope.isUsersFormInvalid(
            // On Succes
            function() {
                var indx = rcUsersFcty.indexOf(user);
                rcUsersFcty.update(user, indx).then(function(success) {
                    if (success) {
                        load(false);
                        $scope.user_was_edited = true;
                        rcNotifier.success('Usuario editado exitosamente');
                        $scope.closeDialog();
                    } else {
                        rcNotifier.error('Usuario no fue editado exitosamente, Inténtelo más tarde');
                        $scope.closeDialog();
                    }
                });

            },
            // On Error
            function(err) {
                rcNotifier.error(err);
            }
        );*/
    };

    $scope.deleteUser = function(user) {
        /* Delete after deployment */
        rcUsersFcty.delete(user);
        /* Uncomment after deployment */
        /*load(true);
        rcUsersFcty.delete(user).then(function(success) {
            load(false);
            if (success) {
                rcNotifier.success('Usuario eliminado exitosamente');
                $scope.closeDeleteUserDialog();
                $scope.closeDialog();
            } else {
                rcNotifier.êrror('Usuario no fue eliminado exitosamente, inténtelo más tarde');
            }
        });*/
    };

    $scope.isUserFormInvalid = function(onSucces, onError) {
        var error = '';

        //Base
        //if ($scope.user_form.attribute.$invalid)
        //    error += '- error to add<br>';

        if ($scope.user_form.$invalid)
            onError(error);
        else
            onSucces();
    };
    
    $scope.initializeModels = function() {
        $scope.current_user = {
            
        };
    };
});