'use strict';

app.controller('rcUsersCtrl', function($scope, $http, rcUserInfoFcty, rcPermissionsFcty, rcHelpersFcty, rcUsersFcty, rcNotifier) {
    //GENERAL
    rcUserInfoFcty.setCurrentModule(5);
    $scope.userInfo = rcUserInfoFcty;
    $scope.sub_menu_sel = 1;
    //USER
    $scope.open_user_dialog = false;
    $scope.user_dialog_opened = 0;
    $scope.users = rcUsersFcty.items;
    $scope.human_resources = rcUsersFcty.human_resources;
    $scope.current_user = {};
    $scope.editable_user = true;
    $scope.previous_user = {};
    $scope.previous_user_index = -1;
    $scope.user_was_edited = false;
    $scope.open_delete_user_dialog = false;
    $scope.hr_selected = false;
    $scope.options = { "search_hr": '' };
    //PERMISSIONS
    $scope.open_permission_dialog = false;
    $scope.permission_dialog_opened = 0;
    $scope.permissions = rcPermissionsFcty;
    $scope.current_permission = {};
    $scope.editable_permission = true;
    $scope.previous_permission = {};
    $scope.previous_permission_index = -1;
    $scope.permission_was_edited = false;
    $scope.open_delete_permission_dialog = false;

    /* GENERAL */

    $scope.Initialize = function() {
        $scope.initializeUsersModels();
        rcUsersFcty.getAllUsers();
        rcUsersFcty.getAllhumanResources();
        rcPermissionsFcty.getAllPermissions();
    };

    $scope.selectOption = function(option) {
        $scope.sub_menu_sel = option;
    };

    $scope.getPermissionName = function(permission) {
        var permission_name = permission.split('"')[3];
        return permission_name;
    };

    /* USERS */

    $scope.openUserDialog = function(id_dialog) {
        $scope.open_user_dialog = true;
        $scope.user_dialog_opened = id_dialog;
    };

    $scope.openNewUserDialog = function() {
        $scope.initializeUsersModels();
        $scope.editable_user = true;
        $scope.openUserDialog(1);
    };

    $scope.openUserDetailDialog = function(user) {
        $scope.current_user = user;
        $scope.editable_user = false;
        user.hr_link = false;
        $scope.current_user.role_name = $scope.getPermissionName(user.role);
        $scope.openUserDialog(2);
        console.log('RH QUE LLEGA');
        console.log(user.id_human_resources);
        if (user.id_human_resources.length !== 0) {
            $scope.options.search_hr = user.id_human_resources[0].name;
            user.hr_link = true;
            $scope.hr_selected = true;
        }
    };

    $scope.openEditableUserDialog = function(user) {
        $scope.closeUserDialog();
        $scope.previous_user_index = rcUsersFcty.indexOf(user);
        $scope.previous_user = rcHelpersFcty.cloner.clone(user);
        $scope.current_user = user;
        $scope.editable_user = true;
        user.hr_link = false;
        $scope.openUserDialog(3);

        if (user.id_human_resources.length !== 0) {
            $scope.options.search_hr = user.id_human_resources[0].name;
            user.hr_link = true;
            $scope.hr_selected = true;
        }
    };

    $scope.openDeleteUserDialog = function() {
        $scope.open_delete_user_dialog = true;
    };

    $scope.closeUserDialog = function() {
        if ($scope.user_dialog_opened == 3 && !$scope.user_was_edited)
            rcUsersFcty.items[$scope.previous_user_index] = $scope.previous_user;

        $scope.open_user_dialog = false;
        $scope.user_was_edited = false;
        $scope.user_dialog_opened = 0;
        $scope.initializeUsersModels();
    };

    $scope.closeDeleteUserDialog = function() {
        $scope.open_delete_user_dialog = false;
    };

    $scope.selectHR = function(hr) {
        $scope.options.search_hr = hr.name;
        $scope.hr_selected = true;
        $scope.current_user.id_hr = hr;
        $scope.current_user.email = hr.email;
    };

    $scope.updateHRLink = function(enable) {
        if (!enable) {
            $scope.current_user.id_hr = '';
            $scope.hr_selected = false;
        }
    };

    $scope.getAllUsers = function() {
        rcUsersFcty.getAllUsers();
    };

    $scope.getAllhumanResources = function() {
        rcUsersFcty.getAllhumanResources();
    };

    $scope.addUser = function(user) {
        load(true);
        $scope.isUserFormInvalid(
            function() {
                user.role = rcPermissionsFcty.getPermissionByName(user.role_name);
                rcUsersFcty.add(user).then(function(success) {
                    load(false);
                    if (success) {
                        rcNotifier.success('Usuario agregado exitosamente.');
                        $scope.closeUserDialog();
                    } else {
                        rcNotifier.error('Error, inténtelo más tarde.');
                        $scope.closeUserDialog();
                    }
                });
            },
            // On Error
            function(err) {
                rcNotifier.error(err);
            }
        );
    };

    $scope.editUser = function(user) {
        load(true);
        $scope.isUserFormInvalid(
            // On Succes
            function() {
                var indx = rcUsersFcty.indexOf(user);
                user.role = rcPermissionsFcty.getPermissionByName(user.role_name);
                rcUsersFcty.update(user, indx).then(function(success) {
                    load(false);
                    if (success) {
                        $scope.user_was_edited = true;
                        rcNotifier.success('Usuario editado exitosamente.');
                        $scope.closeUserDialog();
                    } else {
                        rcNotifier.error('Error, inténtelo más tarde.');
                        $scope.closeUserDialog();
                    }
                });
            },
            // On Error
            function(err) {
                rcNotifier.error(err);
            }
        );
    };

    $scope.deleteUser = function(user) {
        load(true);
        rcUsersFcty.delete(user).then(function(success) {
            load(false);
            if (success) {
                rcNotifier.success('Usuario eliminado exitosamente.');
                $scope.closeDeleteUserDialog();
                $scope.closeUserDialog();
            } else {
                rcNotifier.error('Error, inténtelo más tarde.');
            }
        });
    };

    $scope.isUserFormInvalid = function(onSucces, onError) {
        var error = '';

        if ($scope.user_form.email.$invalid)
            error += '- Correo electrónico es campo requerido ó es incorrecto.<br>';

        if ($scope.user_form.password.$invalid || $scope.user_form.repass.$invalid) {
            if ($scope.user_dialog_opened != 3)
                error += '- Contraseña es campo requerido.<br>';
        } else if ($scope.user_form.password.$modelValue != $scope.user_form.repass.$modelValue)
            error += '- Las contraseñas no coinciden.<br>';

        if ($scope.user_form.role.$invalid)
            error += '- Debe seleccionar un rol.<br>';

        if ($scope.current_user.hr_link && !$scope.hr_selected)
            error += '- Seleccione el recurso humano con el que ligara la cuenta.';

        if (error !== '')
            onError(error);
        else
            onSucces();
    };

    $scope.initializeUsersModels = function() {
        $scope.current_user = {
            "email": "",
            "created_date": "",
            "role": '',
            "id_hr": "",
            "password": "",
            "repass": "",
            "hr_link": false
        };

        $scope.hr_selected = false;
        $scope.options.search_hr = '';
    };

    /* PERMISSIONS */

    $scope.openPermissionDialog = function(id_dialog) {
        $scope.open_permission_dialog = true;
        $scope.permission_dialog_opened = id_dialog;
    };

    $scope.openNewPermissionDialog = function() {
        $scope.initializeUsersModels();
        $scope.editable_permission = true;
        $scope.openPermissionDialog(1);
    };

    $scope.openPermissionDetailDialog = function(permission) {
        $scope.current_permission = permission;
        $scope.editable_permission = false;
        $scope.openPermissionDialog(2);
    };

    $scope.openEditablePermissionDialog = function(permission) {
        $scope.closePermissionDialog();
        $scope.previous_permission_index = rcPermissionsFcty.indexOf(permission);
        $scope.previous_permission = rcHelpersFcty.cloner.clone(permission);
        $scope.current_permission = permission;
        $scope.editable_permission = true;
        $scope.openPermissionDialog(3);
    };

    $scope.openDeletePermissionDialog = function() {
        $scope.open_delete_permission_dialog = true;
    };

    $scope.closePermissionDialog = function() {
        if ($scope.permission_dialog_opened == 3 && !$scope.permission_was_edited)
            rcPermissionsFcty.items[$scope.previous_permission_index] = $scope.previous_permission;

        $scope.open_permission_dialog = false;
        $scope.permission_was_edited = false;
        $scope.permission_dialog_opened = 0;
        $scope.initializePermissionsModels();
    };

    $scope.closeDeletePermissionDialog = function() {
        $scope.open_delete_permission_dialog = false;
    };

    $scope.onSelectModule = function(select) {
        log(select.childNodes);


    };

    $scope.onSelectModule2 = function(select) {
        log(select);
    };

    $scope.getAllPermissions = function() {

    };

    $scope.addPermission = function(permission) {
        /* Delete after deployment */
        $scope.isPermissionFormInvalid(
            // On Succes
            function() { rcPermissionsFcty.add(permission); },
            // On Error
            function(err) { rcNotifier.error(err); }
        );
        /* Uncomment after deployment */
        /*load(true);
        $scope.isPermissionFormInvalid(
            // On Succes
            function() {
                rcPermissionsFcty.add(permission).then(function(success) {
                    load(false);
                    if (success) {
                        rcNotifier.success('Rol agregado exitosamente');
                        $scope.closePermissionDialog();
                    } else {
                        rcNotifier.error('Error, intentelo más tarde');
                        $scope.closePermissionDialog();
                    }
                });
            },
            // On Error
            function(err) {
                rcNotifier.error(err);
            }
        );*/
    };

    $scope.editPermission = function(permission) {
        /* Delete after deployment */
        $scope.isPermissionFormInvalid(
            // On Succes
            function() {
                var indx = rcPermissionsFcty.indexOf(permission);
                rcPermissionsFcty.update(permission, indx);
            },
            // On Error
            function(err) { rcNotifier.error(err); }
        );
        /* Uncomment after deployment and verify functions and variables names */
        /*load(true);
        $scope.isPermissionFormInvalid(
            // On Succes
            function() {
                var indx = rcPermissionsFcty.indexOf(permission);
                rcPermissionsFcty.update(permission, indx).then(function(success) {
                    if (success) {
                        load(false);
                        $scope.permission_was_edited = true;
                        rcNotifier.success('Rol editado exitosamente');
                        $scope.closePermissionDialog();
                    } else {
                        rcNotifier.error('El rol no fue editado exitosamente, Inténtelo más tarde');
                        $scope.closePermissionDialog();
                    }
                });

            },
            // On Error
            function(err) {
                rcNotifier.error(err);
            }
        );*/
    };

    $scope.deletePermission = function(permission) {
        /* Delete after deployment */
        rcPermissionsFcty.delete(permission);
        /* Uncomment after deployment */
        /*load(true);
        rcPermissionsFcty.delete(permission).then(function(success) {
            load(false);
            if (success) {
                rcNotifier.success('Rol eliminado exitosamente');
                $scope.closeDeletePermissionDialog();
                $scope.closePermissionDialog();
            } else {
                rcNotifier.êrror('El Rol no fue eliminado exitosamente, inténtelo más tarde');
            }
        });*/
    };

    $scope.isPermissionFormInvalid = function(onSucces, onError) {
        var error = '';

        //Base
        //if ($scope.permission_form.attribute.$invalid)
        //    error += '- error to add<br>';

        if ($scope.permission_form.$invalid)
            onError(error);
        else
            onSucces();
    };

    $scope.initializePermissionsModels = function() {
        $scope.current_permission = {

        };
    };
});