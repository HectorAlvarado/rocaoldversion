'use strict';

app.factory('rcUsersFcty', function($http, $q, $location, $route) {
    var services = {};
    services.items = [];
    services.human_resources = [];

    services.getAllUsers = function() {
        var obj = {};
        obj.permissions = ['Clients']; // [0] -> MODULO, [1] -> SUBMODULO, [2] -> TAREA
        $http.post('/users/getAllUsers', obj).then(function(response) {
            if (!response.data) {
                $location.path('/');
                $route.reload();
            } else {
                angular.copy(response.data, services.items);
            }
        });
    };

    services.getAllhumanResources = function() {
        var obj = {};
        obj.permissions = ['Clients']; // [0] -> MODULO, [1] -> SUBMODULO, [2] -> TAREA
        $http.post('/users/getAllhumanResources', obj).then(function(response) {
            if (!response.data) {
                $location.path('/');
                $route.reload();
            } else {
                angular.copy(response.data, services.human_resources);
            }
        });
    };

    services.add = function(user) {
        var dfd = $q.defer();

        $http.post('/users/createUser', user).then(function(response) {
            if (response.data) {
                services.items.push(response.data);
                dfd.resolve(true);
            } else {
                dfd.resolve(false);
            }
        });
        return dfd.promise;
    };

    services.delete = function(user) {
        var dfd = $q.defer();

        $http.post('/users/deleteUser', user).then(function(response) {
            if (response.data) {
                var index;
                index = services.items.indexOf(user);
                services.items.splice(index, 1);
                dfd.resolve(true);
            } else {
                dfd.resolve(false);
            }
        });
        return dfd.promise;
    };

    services.update = function(user, index) {
        var dfd = $q.defer();

        $http.post('/users/editUser', user).then(function(response) {
            if (response.data) {
                services.items[index] = response.data;
                dfd.resolve(true);
            } else {
                dfd.resolve(false);
            }
        });
        return dfd.promise;
    };

    services.indexOf = function(user) {
        return services.items.indexOf(user);
    };

    return services;
});