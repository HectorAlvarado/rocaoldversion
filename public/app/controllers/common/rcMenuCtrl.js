'use strict';

app.controller('rcMenuCtrl', function($scope, $location, rcNotifier, rcUserInfoFcty) {

    $scope.selection = rcUserInfoFcty.current_module;
    $scope.userInfo = rcUserInfoFcty;
    console.log('EMAIL');
    
    $scope.selectOption = function(option) {
        switch (option) {
            case 1:
                $location.path('/activo_fijo');
                break;
            case 2:
                $location.path('/Ubicaciones');
                break;
            case 3:
                $location.path('/clientes');
                break;
            case 4:
                $location.path('/recursos_humanos');
                break;
            case 5:
                $location.path('/usuarios');
                break;
        }
    };
});