'use strict';

app.controller('rcUserBarCtrl', function($scope, $location, rcUserInfoFcty, rcNotifier) {
    
    $scope.userInfo = rcUserInfoFcty;
    $scope.open_dialog = false;
    $scope.dialog_opened = 0;
    $scope.current_notification = {};
    $scope.not_content_diplay = false;
    var notifications_to_update = [];

    $scope.Initialize = function() {
        $scope.getNoNotifications();
    };

    $scope.getNoNotifications = function() {
        console.log('NOTIFICATOINES LENGTH');
        console.log(rcUserInfoFcty);
        //console.log(Object.keys(rcUserInfoFcty.notifications).length);
        
        console.log($scope.userInfo.no_notifications);
        if ($scope.userInfo.no_notifications > 9)
            $scope.userInfo.no_notifications = '9+';
        else
            $scope.userInfo.no_notifications = $scope.userInfo.no_notifications;
    };

    $scope.startLogout = function() {
        rcUserInfoFcty.logout().then(function() {            
            $location.path('/inicio_sesion');
            rcNotifier.success('Hasta pronto');
        });
    };

    $scope.openDialog = function(id_dialog) {
        $scope.open_dialog = true;
        $scope.dialog_opened = id_dialog;
        $scope.not_content_diplay = false;
    };

    $scope.closeDialog = function() {
        load(true);
        console.log('UPDATE NOTIFICATIONS ARRAY');
        console.log(notifications_to_update);
        if (notifications_to_update.length === 0) {
            $scope.open_dialog = false;
            $scope.dialog_opened = 0;
            load(false);
        } else {
            rcUserInfoFcty.updateNotifications(rcUserInfoFcty._id, notifications_to_update).then(function(success) {
                if (success) {
                    $scope.open_dialog = false;
                    $scope.dialog_opened = 0;
                    load(false);
                } else {
                    rcNotifier.error('Error. Inténtelo nuevamente.');
                    load(false);
                }
            });
        }
    };

    $scope.viewNotContent = function(notification) {
        $scope.not_content_diplay = true;
        $scope.current_notification = notification;
        if (notification.viewed == false) {
            notification.viewed = true;
            notifications_to_update.push(notification._id);
        }
    };

    $scope.viewNotList = function() {
        $scope.not_content_diplay = false;
        $scope.current_notification = {};
    };

    $scope.deleteNotification = function(notification) {
        load(true);
        var index;   
        var notification_to_delete;
        // delete from the factory array too
        index = rcUserInfoFcty.notifications.indexOf(notification);
        notification_to_delete = rcUserInfoFcty.notifications[index]._id;
        rcUserInfoFcty.notifications.splice(index, 1);
        rcUserInfoFcty.deleteNotifications(rcUserInfoFcty, notification_to_delete).then(function(success) {
            if (success) {
                $scope.getNoNotifications();
                load(false);
            } else {
                rcNotifier.error('Error. Inténtelo nuevamente.');
                load(false);
            }
        });
    };
});