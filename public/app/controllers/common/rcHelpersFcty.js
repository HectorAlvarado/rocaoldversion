app.factory('rcHelpersFcty', function() {
    var helpers = {};

    //Addresses helper implementation
    helpers.addres = {};
    helpers.addres.getSeparateAddress = function(address) {
        var separate_address = address.split(',');

        if (separate_address.length && separate_address.length == 3) {
            separate_address[0] = separate_address[0].trim();
            separate_address[1] = separate_address[1].trim();
            separate_address[2] = separate_address[2].trim();
            return separate_address;
        } else
            return ['Calle', 'Colonia', 'Estado'];
    };

    //Object cloner helper implementation
    helpers.cloner = {};
    helpers.cloner.clone = function(obj) {
        var clon = JSON.parse(JSON.stringify(obj));
        return clon;
    };

    //Regex helper implementation
    helpers.regex = {};
    helpers.regex.phone = /^[\\d()-\\s]+$/;
    helpers.regex.date = /^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/;
    helpers.regex.absolutInteger = /^[0-9]+$/;
    helpers.regex.absolutDecimal = /^[0-9]+(.[0-9]+)?$/;

    //Date helper implementation
    helpers.date = {};
    helpers.date.isValidDate = function(date) {
        var separate_date = date.split('/');
        if (separate_date.length != 3) return false;
        var day = separate_date[0];
        var month = separate_date[1];
        var dayOk = false,
            monthOk = false;

        day = parseInt(day);
        month = parseInt(month);
        dayOk = (day >= 1 && day <= 31);
        monthOk = (month >= 1 && month <= 12);

        return dayOk && monthOk;
    };
    helpers.date.getValidDate = function(date) {
        var separate_date = date.split('/');
        if (separate_date.length != 3) return "dd/mm/aaaa";
        var day = separate_date[0];
        var month = separate_date[1];
        var year = separate_date[2];
        day = day.length == 1 ? "0" + day : day;
        month = month.length == 1 ? "0" + month : month;
        return day + "/" + month + "/" + year;
    };
    helpers.date.getTodayDate = function(){
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; 
        var yyyy = today.getFullYear();
        var today = dd + '/' + mm + '/' + yyyy;
        return helpers.date.getValidDate(today);
    }

    //Follow the same pattern to add a new helper

    return helpers;
});

function log(obj) {
    console.log(obj);
}