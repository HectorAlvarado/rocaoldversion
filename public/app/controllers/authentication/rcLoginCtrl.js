'use strict';

app.controller('rcLoginCtrl', function($scope, $location, rcNotifier, rcUserInfoFcty) {

    //////// Controller and models Initialize ///////

    $scope.Initialize = function() {
        $scope.initializeModels();
    };

    $scope.initializeModels = function() {
        $scope.user = new Object();
        $scope.user.email = "";
        $scope.user.password = "";
    };

    /////////////////////////////////////////////////

    $scope.startLogin = function() {
        load(true);
        if ($scope.loginFormValidation()) {
            rcNotifier.error('Verifique su nombre de usuario y/o contraseña.');
            load(false);
            return;
        }

        rcUserInfoFcty.login($scope.user.email, $scope.user.password).then(function(success) {
            if (success) {
                $location.path('/inicio');
                rcNotifier.success('Bienvenido ' + rcUserInfoFcty.email);
                load(false);
            } else {
                rcNotifier.error('Verifique su nombre de usuario y/o contraseña.');
                load(false);
            }
        });
    };

    $scope.loginFormValidation = function() {
        return $scope.login_form.$invalid;
    };

    $scope.pressEnter = function($event) {
        var keyCode = $event.which || $event.keyCode;
        if (keyCode === 13) {
            $scope.startLogin();
        }
    };

});