'use strict';

app.factory('rcUserInfoFcty', function($http, $q, Upload) {
    var services = {};
    var user = null;

    services.setCurrentModule = function(option) {
        services.current_module = option;
    };

    services.setMenuCollapsed = function(collapsed) {
        services.menu_collapsed = collapsed;
    };

    services.login = function(email, password) {
        var dfd = $q.defer();
        $http.post('/login', {
            username: email,
            password: password
        }).then(function(response) {
            if (response.data.success) {
                console.log(response.data.user);
                services._id = response.data.user._id;
                services.email = response.data.user.email;
                //services.password = password;
                services.current_module = 0;
                services.menu_collapsed = false;
                services.permission = {};
                services.notifications = response.data.user.notifications;
                services.no_notifications = (response.data.user.notifications.filter(function(notifications_viewed) {
                    return notifications_viewed.viewed == false;
                })).length;
                //services.no_notifications = response.data.user.notifications.length;
                user = true;
                dfd.resolve(true);
            } else {
                user = false;
                dfd.resolve(false);
            }
        });
        return dfd.promise;
    };

    services.logout = function(email, password) {
        var dfd = $q.defer();
        $http.get('/logout')
            .success(function(data) {
                user = false;
                dfd.resolve();
            })
            .error(function(data) {
                user = false;
                dfd.reject();
            });
        return dfd.promise;
    };

    services.isLoggedIn = function() {
        if (user) {
            return true;
        } else {
            return false;
        }
    };

    services.getUserStatus = function() {
        return $http.get('/status')
            // handle success
            .success(function(data) {
                if (data.status) {
                    user = true;
                    services._id = data.userInfo._id;
                    services.email = data.userInfo.email;
                    //services.password = data.userInfo.password;
                    services.current_module = 0;
                    services.menu_collapsed = false;
                    services.permission = {};
                    services.notifications = data.userInfo.notifications;
                    services.no_notifications = (data.userInfo.notifications.filter(function(notifications_viewed) {
                        return notifications_viewed.viewed == false;
                    })).length;
                    //services.no_notifications = data.userInfo.notifications.length;
                    console.log('REGRESO DE STATUS');
                    console.log(services);
                } else {
                    user = false;
                }
            })
            // handle error
            .error(function(data) {
                user = false;
            });
    };

    services.updateNotifications = function(userInfo, notifications_to_update) {
        var dfd = $q.defer();
        $http.post('/updateNotifications', {
            userInfo: userInfo,
            notifications_to_update: notifications_to_update
        }).then(function(response) {
            console.log('RESPUESTA');
            console.log(response);
            if (response.data) {
                if (response.data != 'NO') {
                    services.notifications = response.data.notifications;
                    services.no_notifications = (response.data.notifications.filter(function(notifications_viewed) {
                        return notifications_viewed.viewed == false;
                    })).length;
                    console.log(services);
                }
                dfd.resolve(true);
            } else {
                dfd.resolve(false);
            }
        });
        return dfd.promise;
    }

    services.deleteNotifications = function(userInfo, notification_to_delete) {
        var dfd = $q.defer();
        $http.post('/deleteNotifications', {
            userInfo: userInfo,
            notification_to_delete: notification_to_delete
        }).then(function(response) {
            if (response.data) {
                console.log('RESPUESTA');
                console.log(response.data);
                services.notifications = response.data.notifications;
                services.no_notifications = (response.data.notifications.filter(function(notifications_viewed) {
                    return notifications_viewed.viewed == false;
                })).length;
                //services.no_notifications = response.data.notifications.length;
                console.log(services);
                dfd.resolve(true);
            } else {
                dfd.resolve(false);
            }
        });
        return dfd.promise;
    }

    return services;
});