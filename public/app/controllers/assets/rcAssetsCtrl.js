'use strict';

app.controller('rcAssetsCtrl', function($scope, $http, rcUserInfoFcty, rcAssetsFcty, rcHelpersFcty, rcAssetsSrc, rcNotifier, rcClientsFcty) {

    rcUserInfoFcty.setCurrentModule(1);
    $scope.userInfo = rcUserInfoFcty;
    $scope.assets = rcAssetsFcty;
    $scope.category_options = rcAssetsSrc.category_options;
    $scope.subcategory_options = [];
    $scope.subcategories = rcAssetsSrc.subcategories;
    $scope.clients = rcClientsFcty.getAllClientsForRentSale();
    $scope.sub_menu_sel = 1;
    $scope.regex = rcHelpersFcty.regex;
    $scope.open_dialog = false;
    $scope.dialog_opened = 0;
    $scope.rsl_dialog_opened = 0;
    $scope.current_asset = {};
    $scope.editable_asset = true;
    $scope.previous_asset = {};
    $scope.op = { "images_files": [] };
    $scope.previous_asset_index = -1;
    $scope.asset_was_edited = false;
    $scope.open_rent_sale_lease_dialog = false;
    $scope.documents = rcAssetsSrc.documents;
    $scope.documents_keys = rcAssetsSrc.documents_keys;
    $scope.sale = {};
    $scope.rent_lease = {};

    $scope.Initialize = function() {
        $scope.initializeModels();
        $scope.getAllAssets();
        $scope.initializeRentLeaseModels();
    };

    $scope.selectOption = function(option) {
        $scope.sub_menu_sel = option;
    };

    $scope.openDialog = function(id_dialog) {
        $scope.open_dialog = true;
        $scope.dialog_opened = id_dialog;
    };

    $scope.openNewAssetDialog = function() {
        $scope.initializeModels();
        $scope.openDialog(1);
        $scope.editable_asset = true;
    };

    $scope.openAssetDetailDialog = function(asset) {
        $scope.current_asset = asset;
        $scope.current_asset.fuel_used = asset.engine_assets_id !== '';
        $scope.photo_1 = { 'background-image': 'url(' + asset.url_image_one + ')' };
        $scope.photo_2 = { 'background-image': 'url(' + asset.url_image_two + ')' };
        $scope.photo_3 = { 'background-image': 'url(' + asset.url_image_three + ')' };
        $scope.photo_4 = { 'background-image': 'url(' + asset.url_image_four + ')' };
        $scope.editable_asset = false;
        $scope.openDialog(2);
        $scope.onCategorySelected(asset.category);
    };

    $scope.openEditableAssetDialog = function(asset) {
        $scope.closeDialog();
        $scope.previous_asset_index = rcAssetsFcty.indexOf(asset);
        $scope.previous_asset = rcHelpersFcty.cloner.clone(asset);
        $scope.current_asset = asset;
        $scope.editable_asset = true;
        $scope.openDialog(3);
    };

    $scope.closeDialog = function() {
        if ($scope.dialog_opened == 3 && !$scope.asset_was_edited)
            rcAssetsFcty.items[$scope.previous_asset_index] = $scope.previous_asset;

        $scope.open_dialog = false;
        $scope.asset_was_edited = false;
        $scope.dialog_opened = 0;
        $scope.initializeModels();
    };

    $scope.openRentSaleLeaseAssetDialog = function() {
        $scope.open_rent_sale_lease_dialog = true;
    };

    $scope.closeRentSaleLeaseAssetDialog = function() {
        $scope.open_rent_sale_lease_dialog = false;
        $scope.rsl_dialog_opened = 0;
        $scope.initializeRentLeaseModels();
    };

    $scope.openRentAssetDialog = function() {
        $scope.rsl_dialog_opened = 1;
        $scope.openRentSaleLeaseAssetDialog();

        if ($scope.current_asset.possession_status == 'renta')
            $scope.rent_lease = rcAssetsFcty.getAssetRentData($scope.current_asset.id);
    };

    $scope.openSaleAssetDialog = function() {
        $scope.rsl_dialog_opened = 2;
        $scope.openRentSaleLeaseAssetDialog();
    };

    $scope.openLeaseAssetDialog = function() {
        $scope.rsl_dialog_opened = 3;
        $scope.openRentSaleLeaseAssetDialog();
    };

    $scope.onCategorySelected = function(selection) {
        $scope.subcategory_options = [];

        for (var key in $scope.subcategories[selection])
            $scope.subcategory_options.push(key);
    };

    $scope.onFileTypeSelected = function() {
        var indx = $scope.documents.indexOf($scope.current_file.documentSelected);
        var key = $scope.documents_keys[indx];
        $scope.current_file.uploaded = false;

        if (typeof($scope.current_asset[key]) == 'string') {
            $scope.current_file.uploaded = $scope.current_asset[key] != '';
            $scope.current_file.name = "No hay archivo seleccionado";
            $scope.current_file.selected = false;
        } else if ($scope.current_asset[key] == null) {
            $scope.current_file.name = "No hay archivo seleccionado";
            $scope.current_file.selected = false;
        } else {
            $scope.current_file.selected = true;
            $scope.current_file.name = $scope.current_asset[key].name;
        }
    };

    $scope.onFileDataSelected = function(file) {
        var indx = $scope.documents.indexOf($scope.current_file.documentSelected);
        var key = $scope.documents_keys[indx];

        if (file == null) {
            $scope.current_file.name = "No hay archivo seleccionado";
            $scope.current_file.selected = false;
            return;
        }

        $scope.current_file.name = file.name;
        $scope.current_file.selected = true;
        $scope.current_asset[key] = file;
    };

    $scope.onRemoveFileSelected = function() {
        var indx = $scope.documents.indexOf($scope.current_file.documentSelected);
        var key = $scope.documents_keys[indx];
        $scope.current_file.name = "No hay archivo seleccionado";
        $scope.current_file.selected = false;
        $scope.current_file.data = null;
        $scope.current_asset[key] = null;
    };

    $scope.normalizeAssetFields = function(asset) {
        //Normalize null files
        for (var i = 0; i < $scope.documents_keys.length; i++) {
            if (asset[$scope.documents_keys[i]] == null)
                asset[$scope.documents_keys[i]] = "";
        }
        //Normalize dates
        asset.created_date = rcHelpersFcty.date.getValidDate(asset.created_date);
        asset.status = 'almacen';

        return asset;
    };

    $scope.downloadDocument = function() {
        var indx = $scope.documents.indexOf($scope.current_file.documentSelected);
        var key = $scope.documents_keys[indx];
        if (typeof($scope.current_asset[key]) == 'string')
            goInNewTab($scope.current_asset[key]);
    };

    $scope.getAssetName = function(possession_status, category, subcategory) {
        var name = '';
        switch (possession_status) {
            case 'rentado':
            case 'propio':
                name = 'ROCA';
                break;
            case 'arrendado':
                name = "ARRE";
                break;
        }
        name += $scope.subcategories[category][subcategory];
        return name;
    };

    $scope.onTypePaymentSelected = function(selection) {
        //
    }; ///

    $scope.rentAsset = function(asset, rent_info) {
        $scope.isRentLeaseFormValid(
            function() {
                rent_info.created_date = rcHelpersFcty.date.getValidDate(rent_info.created_date);
                rent_info.rent_lease = "rent";
                rent_info.id_asset = asset.id;

                asset.possession_status = 'renta';

                rcAssetsFcty.rentAsset(asset, rent_info).then(function(success) {
                    if (success) {
                        rcNotifier.success('Activo fijo en renta');
                        $scope.closeRentSaleLeaseAssetDialog();
                    } else {
                        rcNotifier.error('Error con el servidor, intentelo mas tarde');
                        $scope.closeRentSaleLeaseAssetDialog();
                    }
                });
            },
            function(error) {
                rcNotifier.error(error);
            }
        );
    };

    $scope.finishRent = function(rent_data, asset) {
        asset.possession_status = "propio";
        rcAssetsFcty.finishAssetRent(asset).then(function(success) {
            if (success) {
                rcNotifier.success('Activo fijo rentado ahora esta en almacen');
                $scope.closeRentSaleLeaseAssetDialog();
            } else {
                rcNotifier.error('Error con el servidor, intentelo mas tarde');
                $scope.closeRentSaleLeaseAssetDialog();
            }
        });
    };

    $scope.sellAsset = function(sale, current_asset) {
        rcAssetsFcty.sellAsset(sale, current_asset).then(function(success) {
            if (success) {
                rcNotifier.success('Activo fijo vendido');
                $scope.closeRentSaleLeaseAssetDialog();
            } else {
                rcNotifier.error('Error con el servidor, intentelo mas tarde');
                $scope.closeRentSaleLeaseAssetDialog();
            }
        });
    };

    $scope.leaseAsset = function(current_asset, rent_lease) {
        $scope.isRentLeaseFormValid(
            function() {
                rcAssetsFcty.leaseAsset(current_asset, rent_lease).then(function(success) {
                    if (success) {
                        rcNotifier.success('Activo fijo arrendado');
                        $scope.closeRentSaleLeaseAssetDialog();
                    } else {
                        rcNotifier.error('Error con el servidor, intentelo mas tarde');
                        $scope.closeRentSaleLeaseAssetDialog();
                    }
                });
            },
            function(error) {
                rcNotifier.error(error);
            }
        );
    };

    $scope.isRentLeaseFormValid = function(onSuccess, onError) {
        var error = '';

        if ($scope.asset_dialog.rent_lease_price.$invalid)
            error += '- Precio es campo requerido o no es un formato valido';

        if ($scope.asset_dialog.rent_lease_created_date.$invalid)
            error += '- Fecha es campo requerido o el formato es incorrecto (dd/mm/yyyy)<br>';

        if (error != '') onError(error);
        else onSuccess();
    };

    $scope.getAllAssets = function() {
        rcAssetsFcty.getAllAssets();
    };

    $scope.addAsset = function(asset, images) {
        $scope.isAssetFormInvalid(
            // on succes form valid
            function() {
                load(true);
                asset.name = $scope.getAssetName(asset.possession_status, asset.category, asset.subcategory);
                asset = $scope.normalizeAssetFields(asset);
                rcAssetsFcty.add(asset, images).then(function(success) {
                    if (success) {
                        $scope.closeDialog();
                        rcNotifier.success('Activo fijo agregado con exito');
                        load(false);
                    } else {
                        rcNotifier.error('Error, intentelo más tarde');
                        $scope.closeDialog();
                    }
                });
            },
            // on fail form valid
            function(error) {
                rcNotifier.error(error);
            }
        );
    };

    $scope.editAsset = function(asset, images) {
        $scope.isAssetFormInvalid(
            // On Succes
            function() {
                var indx = rcAssetsFcty.items.indexOf(asset);
                rcAssetsFcty.update(asset, images, indx);
                $scope.asset_was_edited = true;
                rcNotifier.success('Activo fijo editado exitosamente');
                $scope.closeDialog();
            },
            // On Error
            function(err) {
                rcNotifier.error(err);
            }
        );
    };

    $scope.isAssetFormInvalid = function(onSucces, onError) {
        var error = '';

        if ($scope.asset_dialog.category.$invalid)
            error += '- Elija categoría de activo fijo<br>';
        if ($scope.asset_dialog.subcategory.$invalid)
            error += '- Elija tipo de activo fijo<br>';
        if ($scope.asset_dialog.brand.$invalid)
            error += '- Maraca es campo requerido<br>';
        if ($scope.asset_dialog.year.$invalid)
            error += '- Año es campo requerido o el formato es incorrecto<br>';
        if ($scope.asset_dialog.model.$invalid)
            error += '- Modelo es campo requerido<br>';
        if ($scope.asset_dialog.created_date.$invalid)
            error += '- Fecha es campo requerido o el formato es incorrecto (dd/mm/yyyy)<br>';

        if ($scope.current_asset.fuel_used) {
            if ($scope.asset_dialog.fuel_type.$invalid)
                error += '- Elija un tipo de combustible<br>';

            if ($scope.asset_dialog.fuel_tank_size.$invalid)
                error += '- Tamaño de tanque es campo requerido<br>';
        }
        if (error != "")
            onError(error);
        else
            onSucces();
    };

    $scope.initializeModels = function() {
        $scope.current_asset = {
            "name": "",
            "category": "",
            "subcategory": "",
            "description": "",
            "brand": "",
            "model": "",
            "year": "",
            "serial_number": "",
            "created_date": rcHelpersFcty.date.getTodayDate(),
            "possession_status": "propio",
            "working_status": "almacen",
            "fuel_used": false,
            "engine_assets_id": {
                "fuel_type": "",
                "fuel_tank_size": "",
                "plates": ""
            },
            "url_image_one": "",
            "url_image_two": "",
            "url_image_three": "",
            "url_image_four": "",
            "doc_guard_asset": "",
            "doc_bill_asset ": "",
            "doc_insurance_asset": "",
            "doc_motion_asset": ""
        };

        $scope.op.images_files = [];

        $scope.current_file = {
            "documentSelected": "",
            "name": "No hay archivo seleccionado",
            "selected": false,
            "uploaded": false,
            "data": null
        };

        $scope.photo_1 = { 'background-image': '' };
        $scope.photo_2 = { 'background-image': '' };
        $scope.photo_3 = { 'background-image': '' };
        $scope.photo_4 = { 'background-image': '' };
    };

    $scope.initializeRentLeaseModels = function() {
        $scope.sale = {
            "id_asset": "",
            "paymetn_type": "1",
            "total_price": "",
            "partial_payment": "",
            "created_date": rcHelpersFcty.date.getTodayDate(),
            "lapse": "",
            "no_payment": "",
            "final_date": "",
            "client": "undefined",
            "description": ""
        };

        $scope.rent_lease = {
            "rent_lease": "",
            "price": "",
            "lapse": "4",
            "created_date": rcHelpersFcty.date.getTodayDate(),
            "client_provider": "",
            "description": ""
        };
    };
});