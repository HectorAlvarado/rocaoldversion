'use strict';

app.factory('rcAssetsFcty', function($http, $q, Upload) {
    var services = {};

    services.items = [];

    services.getAllAssets = function() {
        $http.get('/asset/getAllAssets').then(function(response) {
            angular.copy(response.data, services.items);
        });
    };

    services.add = function(asset, images) {
        var dfd = $q.defer();

        Upload.upload({
            url: '/asset/addAsset',
            data: {
                newAsset: asset,
                file: {
                    url_image_one: images[0],
                    url_image_two: images[1],
                    url_image_three: images[2],
                    url_image_four: images[3],
                    doc_guard_asset: asset.doc_guard_asset,
                    doc_bill_asset: asset.doc_bill_asset,
                    doc_insurance_asset: asset.doc_insurance_asset,
                    doc_motion_asset: asset.doc_motion_asset
                }
            }
        }).then(function(response) {
            if (response) {
                services.items.push(response.data);
                dfd.resolve(true);
            } else {
                dfd.resolve(false);
            }
        }, function(resp) {
            console.log('Error status: ' + resp.status);
        });
        return dfd.promise;
    };

    services.delete = function(asset) {
        var index;
        index = services.items.indexOf(asset);
        services.items.splice(index, 1);
        return services;
    };

    services.update = function(asset, images, index) {
        var dfd = $q.defer();

        Upload.upload({
            url: '/asset/editAsset',
            data: {
                newAsset: asset,
                file: {
                    url_image_one: images[0],
                    url_image_two: images[1],
                    url_image_three: images[2],
                    url_image_four: images[3],
                    doc_guard_asset: asset.doc_guard_asset,
                    doc_bill_asset: asset.doc_bill_asset,
                    doc_insurance_asset: asset.doc_insurance_asset,
                    doc_motion_asset: asset.doc_motion_asset
                }
            }
        });
    };

    services.rentAsset = function(asset, rent_info) {
        console.log(asset);
        var dfd = $q.defer();

        var obj = {};
        obj.asset = asset;
        obj.rent_info = rent_info;

        $http.post('/asset/rentAsset', obj).then(function(response) {
            if (response) {
                console.log('info from factory of the asset that has been rent...');
                console.log(response);
                dfd.resolve(true);
            } else {
                dfd.resolve(false);
            }
        });
        return dfd.promise;
    };

    services.finishAssetRent = function(asset) {
        var dfd = $q.defer();

        var obj = {};
        obj.rent_data = asset;

        $http.post('/asset/finishRentAsset', obj).then(function(response) {
            if (response) {
                console.log(response);
                dfd.resolve(true);
            } else {
                dfd.resolve(false);
            }
        });
        return dfd.promise;
    };

    services.sellAsset = function(assetToSale, current_asset) {
        var dfd = $q.defer();

        var obj = {};

        obj.assetToSell = assetToSale;
        obj.current_asset = current_asset;

        $http.post('/asset/sellAsset', obj).then(function(response) {
            if (response) {
                var index;
                index = services.items.indexOf(current_asset);
                services.items.splice(index, 1);
                dfd.resolve(true);
            } else {
                dfd.resolve(false);
            }
        });
        return dfd.promise;
    };

    services.leaseAsset = function(current_asset, rent_lease) {
        var dfd = $q.defer();

        var obj = {};

        obj.current_asset = current_asset;
        obj.rent_lease = rent_lease;

        $http.post('/asset/leaseAsset', obj).then(function(response) {
            if (response) {
                dfd.resolve(true);
            } else {
                dfd.resolve(false);
            }
        });
        return dfd.promise;
    };



    services.indexOf = function(asset) {
        return services.items.indexOf(asset);
    };

    services.getAssetRentData = function(id) {
        var rent_data = {
            "id_asset": "1fsd4adf5sdsf",
            "rent_lease": "rent",
            "price": "1250",
            "lapse": "4",
            "created_date": "07/10/2016",
            "client_provider": {
                "id": "1fad234a234fee",
                "name": "Aluvipesa",
                "category": "Herrería"
            },
            "description": "Renta de excavadora caterpillar"
        };

        return rent_data;
    };

    return services;
});