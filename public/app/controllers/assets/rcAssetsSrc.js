app.factory('rcAssetsSrc', function() {
    var asset_sources = {};

    asset_sources.category_options = [
        "Maquinaria Pesada",
        "Maquinaria Ligera",
        "Equipo de Transporte",
        "Equipo de Computo",
        "Accesorio",
        "Remolque",
        "Utillaje",
        "Señaletica",
        "Equipo Topográfico",
        "Equipo de Seguridad",
        "Herramienta",
        "otro"
    ];
    
    asset_sources.subcategories = {
        "Maquinaria Pesada"     : {
            "Excavadora" :      "REX",
            "Buldocer"   :      "BLC",
            "Retroexcavadora" : "REX"
        },
        "Maquinaria Ligera"     : {
            "ml_1" : "ML1",
            "ml_2" : "ML2",
            "ml_3" : "ML3"
        },
        "Equipo de Transporte"  : {
            "edt_1" : "ET1",
            "edt_2" : "ET2",
            "edt_3" : "ET3"
        },
        "Equipo de Computo"     : {
            "edc_1" : "EC1",
            "edc_2" : "EC2",
            "edc_3" : "EC3"
        },
        "Accesorio"             : {
            "ac_1" : "AC1",
            "ac_2" : "AC2",
            "ac_3" : "AC3"
        },
        "Remolque"              : {
            "rq_1" : "RQ1",
            "rq_2" : "RQ2",
            "rq_3" : "RQ3"
        },
        "Utillaje"              : {
            "ut_1" : "UT1",
            "ut_2" : "UT2",
            "ut_3" : "UT3"
        },
        "Señaletica"            : {
            "sl_1" : "SL1",
            "sl_2" : "SL2",
            "sl_3" : "SL3"
        },
        "Equipo Topográfico"    : {
            "et_1" : "ET1",
            "et_2" : "ET2",
            "et_3" : "ET3"
        },
        "Equipo de Seguridad"   : {
            "es_1" : "ES1",
            "es_2" : "ES2",
            "es_3" : "ES3"
        },
        "Herramienta"           : {
            "hr_1" : "HR1",
            "hr_2" : "HR2",
            "hr_3" : "HR3"
        },
        "otro"                  : {
            "ot_1" : "OT1",
            "ot_2" : "OT2",
            "ot_3" : "OT3"
        }
    };
    
    asset_sources.documents = [
        "Resguardo",
        "Factura",
        "Seguro",
        "Pedimento"
    ];
    
    asset_sources.documents_keys = [
        "doc_guard_asset",
        "doc_bill_asset ",
        "doc_insurance_asset",
        "doc_motion_asset"
    ];

    return asset_sources;
});