'use strict';

app.controller('rcHumanResourceCtrl', function($scope, $http, rcUserInfoFcty, rcHumanResourceFcty, rcNotifier, rcHelpersFcty, rcCommonSrc, rcHumanResourceSrc, Upload) {

    rcUserInfoFcty.setCurrentModule(4);
    $scope.userInfo = rcUserInfoFcty;
    $scope.humanResource = rcHumanResourceFcty;
    $scope.regex = rcHelpersFcty.regex;
    $scope.sub_menu_sel = 1;
    $scope.open_dialog = false;
    $scope.editable_human_resource = true;
    $scope.dialog_opened = 0;
    $scope.current_human_resource = {};
    $scope.previous_humnan_resource = {};
    $scope.final_image = {};
    $scope.previous_humnan_resource_index = -1;
    $scope.humnan_resource_was_edited = false;
    $scope.states = rcCommonSrc.states;
    $scope.documents = rcHumanResourceSrc.documents;
    $scope.documents_keys = rcHumanResourceSrc.documents_keys;
    $scope.departments = rcHumanResourceSrc.departments;
    $scope.open_delete_hr_dialog = false;
    $scope.current_file = {};
    $scope.delete_description = "";

    $scope.Initialize = function() {
        $scope.initializeModels();
        $scope.getAllHumanResources();
    };

    $scope.selectOption = function(option) {
        $scope.sub_menu_sel = option;
    };

    $scope.openDialog = function(id_dialog) {
        $scope.open_dialog = true;
        $scope.dialog_opened = id_dialog;
        $scope.current_file.documentSelected = "";
    };

    $scope.openNewHumanResourceDialog = function() {
        $scope.initializeModels();
        $scope.editable_human_resource = true;
        $scope.openDialog(1);
    };

    $scope.openHumanResourceDetailDialog = function(human_resource) {
        var address = rcHelpersFcty.addres.getSeparateAddress(human_resource.address);
        $scope.current_human_resource = human_resource;

        $scope.current_human_resource.address_parts = {};
        $scope.current_human_resource.address_parts.street = address[0];
        $scope.current_human_resource.address_parts.suburb = address[1];
        $scope.current_human_resource.address_parts.state = address[2];
        $scope.photo = {
            'background-image': 'url(' + human_resource.photo + ')'
        };
        $scope.editable_human_resource = false;
        $scope.openDialog(2);
    };

    $scope.openEditableHumanResourceDialog = function(human_resource) {
        $scope.closeDialog();
        $scope.previous_humnan_resource_index = rcHumanResourceFcty.indexOf(human_resource);
        $scope.previous_humnan_resource = rcHelpersFcty.cloner.clone(human_resource);
        $scope.current_human_resource = human_resource;
        $scope.editable_human_resource = true;
        $scope.openDialog(3);
    };

    $scope.openDeleteHRDialog = function() {
        $scope.open_delete_hr_dialog = true;
    }

    $scope.closeDeleteHRDialog = function() {
        $scope.open_delete_hr_dialog = false;
        $scope.current_human_resource.status = "contratado";
        $scope.delete_description = "";
    }

    $scope.closeDialog = function() {
        if ($scope.dialog_opened == 3 && !$scope.humnan_resource_was_edited)
            rcHumanResourceFcty.items[$scope.previous_humnan_resource_index] =
            $scope.previous_humnan_resource;

        $scope.open_dialog = false;
        $scope.humnan_resource_was_edited = false;
        $scope.dialog_opened = 0;
        $scope.initializeModels();
    };

    $scope.getAllHumanResources = function() {
        rcHumanResourceFcty.getAllHumanResources();
    };

    $scope.addHumanResource = function(human_resource) {
        $scope.isHumanResourceFormInvalid(
            function() {
                load(true);
                human_resource = $scope.normalizeHumanResourceFields(human_resource);
                rcHumanResourceFcty.add(human_resource).then(function(success) {
                    if (success) {
                        $scope.closeDialog();
                        rcNotifier.success('Recurso humano agregado exitosamente');
                        load(false);
                    } else {
                        load(false);
                        rcNotifier.error('Error, intentelo más tarde');
                        $scope.closeDialog();
                    }
                });
            },
            function(error) {
                rcNotifier.error(error);
            }
        );
    };

    $scope.normalizeHumanResourceFields = function(human_resource) {
        //Normalize null files
        for (var i = 0; i < $scope.documents_keys.length; i++) {
            if (human_resource[$scope.documents_keys[i]] == null)
                human_resource[$scope.documents_keys[i]] = "";
        }
        //Normalize dates
        human_resource.birthday = rcHelpersFcty.date.getValidDate(human_resource.birthday);
        //Normalize url image
        if ($scope.final_image.file != "")
            human_resource.file_image = Upload.dataUrltoBlob($scope.final_image.base64, $scope.final_image.file.name);

        return human_resource;
    };

    $scope.editHumanResource = function(human_resource) {
        $scope.isHumanResourceFormInvalid(
            // On succes form validation
            function() {
                load(true);
                var indx = rcHumanResourceFcty.items.indexOf(human_resource);
                $scope.humnan_resource_was_edited = true;
                human_resource = $scope.normalizeHumanResourceFields(human_resource);
                rcHumanResourceFcty.update(human_resource, indx).then(function(success) {
                    if (success) {
                        load(false);
                        rcNotifier.success('Recurso humano editado exitosamente');
                        $scope.closeDialog();
                    } else {
                        load(false);
                        rcNotifier.error('Recurso humano no fue editado exitosamente');
                        $scope.closeDialog();
                    }
                });
            },
            // On error form validation
            function(error) {
                rcNotifier.error(error);
            }
        );
    };

    $scope.deleteHumanResource = function(human_resource, description) {
        load(true);
        if (human_resource.status == "contratado" || description.trim() == "") {
            rcNotifier.error("Debe elegir una opción y/o una descripción");
            return;
        } else {
            rcHumanResourceFcty.delete(human_resource, description).then(function(success) {
                if (success) {
                    load(false);
                    rcNotifier.success('Recurso humano dado de baja exitosamente');
                    $scope.closeDialog();
                } else {
                    load(false);
                    rcNotifier.error('Recurso humano  no fue dado de baja exitosamente');
                    $scope.closeDialog();
                }
            });
        }
    };

    $scope.isHumanResourceFormInvalid = function(onSucces, onError) {
        var error = '';

        if ($scope.human_resource_dialog.name.$invalid)
            error += '- Nombre es campo requerido<br>';
        if ($scope.human_resource_dialog.gender.$invalid)
            error += '- Genero es campo requerido<br>';
        if ($scope.human_resource_dialog.department.$invalid)
            error += '- Departamento es campo requerido<br>';
        if ($scope.human_resource_dialog.curp.$invalid)
            error += '- Curp es campo requerido<br>';
        if ($scope.human_resource_dialog.rfc.$invalid)
            error += '- Departamento es campo requerido<br>';
        if ($scope.human_resource_dialog.social_security_number.$invalid)
            error += '- Número de seguro es campo requerido<br>';
        if ($scope.human_resource_dialog.phone.$invalid)
            error += '- Teléfono es campo requerido o no es valido<br>';
        if ($scope.human_resource_dialog.email.$invalid)
            error += '- Correo Electrónico es campo requerido o no es valido<br>';
        if ($scope.human_resource_dialog.birthday.$invalid)
            error += '- Fecha de nacimiento es campo requerido o no es valido (dd/mm/aaaa)<br>';
        else if (!rcHelpersFcty.date.isValidDate($scope.human_resource_dialog.birthday.$modelValue))
            error += '- Verifique los valores de día y mes de fecha de nacimiento<br>';
        if ($scope.human_resource_dialog.nationality.$invalid)
            error += '- Nacionalidad es campo requerido<br>';
        if ($scope.human_resource_dialog.marital_status.$invalid)
            error += '- Estado civil es campo requerido<br>';
        if ($scope.human_resource_dialog.street.$invalid ||
            $scope.human_resource_dialog.suburb.$invalid ||
            $scope.human_resource_dialog.street.state)
            error += '- Dirección es campo requerido<br>';

        if ($scope.human_resource_dialog.$invalid)
            onError(error);
        else
            onSucces();
    };

    $scope.onFileTypeSelected = function() {
        var indx = $scope.documents.indexOf($scope.current_file.documentSelected);
        var key = $scope.documents_keys[indx];
        $scope.current_file.uploaded = false;

        if (typeof($scope.current_human_resource[key]) == 'string') {
            $scope.current_file.uploaded = $scope.current_human_resource[key] != '';
            $scope.current_file.name = "No hay archivo seleccionado";
            $scope.current_file.selected = false;
        } else if ($scope.current_human_resource[key] == null) {
            $scope.current_file.name = "No hay archivo seleccionado";
            $scope.current_file.selected = false;
        } else {
            $scope.current_file.selected = true;
            $scope.current_file.name = $scope.current_human_resource[key].name;
        }
    };

    $scope.onFileDataeSelected = function(file) {
        var indx = $scope.documents.indexOf($scope.current_file.documentSelected);
        var key = $scope.documents_keys[indx];

        if (file == null) {
            $scope.current_file.name = "No hay archivo seleccionado";
            $scope.current_file.selected = false;
            return;
        }

        $scope.current_file.name = file.name;
        $scope.current_file.selected = true;
        $scope.current_human_resource[key] = file;
    };

    $scope.onRemoveFileSelected = function() {
        var indx = $scope.documents.indexOf($scope.current_file.documentSelected);
        var key = $scope.documents_keys[indx];
        $scope.current_file.name = "No hay archivo seleccionado";
        $scope.current_file.selected = false;
        $scope.current_file.data = null;
        $scope.current_human_resource[key] = null;
    };

    $scope.downloadDocument = function() {
        var indx = $scope.documents.indexOf($scope.current_file.documentSelected);
        var key = $scope.documents_keys[indx];
        if (typeof($scope.current_human_resource[key]) == 'string')
            goInNewTab($scope.current_human_resource[key]);
    };

    $scope.initializeModels = function() {
        $scope.current_human_resource = {
            "name": "",
            "department": "",
            "curp": "",
            "social_security_number": "",
            "phone": "",
            "email": "",
            "birthday": "",
            "nationality": "Mexicano(a)",
            "marital_status": "Soltero(a)",
            "description": "",
            "url_image": "",
            "file_image": "",
            "address": "",
            "address_parts": {
                "street": "",
                "suburb": "",
                "state": "San Luis Potosí"
            },
            "gender": "Hombre",
            "rfc": "",
            "location_id": "to change",
            "final_date": "",
            "status": "contratado",
            "doc_birth_certificate": "",
            "doc_ine": "",
            "doc_curp": "",
            "doc_proof_of_address": "",
            "doc_rfc": "",
            "doc_curricula": "",
            "doc_proof_of_studies": "",
            "doc_withholding_notice_infonavit": "",
            "doc_social_security_number": "",
            "doc_driver_license": "",
            "doc_recomendation_letter": "",
            "doc_labor_recomendation_letter": "",
            "doc_letter_of_no_criminal_record": "",
            "doc_medical_certificate": "",
            "doc_medical_history": "",
            "doc_antidoping_test": ""
        };

        $scope.current_file = {
            "documentSelected": "",
            "name": "No hay archivo seleccionado",
            "selected": false,
            "uploaded": false,
            "data": null
        }

        $scope.final_image = {
            "base64": "",
            file: ""
        };

        $scope.delete_description = "";
    };
});