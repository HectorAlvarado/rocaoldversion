app.factory('rcHumanResourceSrc', function() {
    var hr_sources = {};

    hr_sources.documents = [
        "Acta de nacimiento",
        "Credencial de elector",
        "Curp",
        "Comprobante de domicilio",
        "RFC",
        "Curriculum o solicitud",
        "Constancia de estudios",
        "Aviso de retención de INFONAVIT",
        "Número de seguro social (IMSS)",
        "Licencia de manejo",
        "Carta de recomendación personal",
        "Carta de recomendación laboral",
        "Carta de antecedentes no penales",
        "Certificado médico",
        "Historial médico",
        "Prueba toxicológica antidoping",
    ];
    
    hr_sources.documents_keys = [
        "doc_birth_certificate",
        "doc_ine",
        "doc_curp",
        "doc_proof_of_address",
        "doc_rfc",
        "doc_curricula",
        "doc_proof_of_studies",
        "doc_withholding_notice_infonavit",
        "doc_social_security_number",
        "doc_driver_license",
        "doc_recomendation_letter",
        "doc_labor_recomendation_letter",
        "doc_letter_of_no_criminal_record",
        "doc_medical_certificate",
        "doc_medical_history",
        "doc_antidoping_test"  
    ];
    
    hr_sources.departments = [
        "Sistemas",
        "Recursos humanos",
        "Ventas"
    ];
    
    return hr_sources;
});