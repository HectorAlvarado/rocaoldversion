'use strict';

app.factory('rcHumanResourceFcty', function($http, $q, Upload) {
    var services = {};

    services.items = [];

    services.getAllHumanResources = function() {
        $http.get('/humanResource/getAllHumanResources').then(function(response) {
            angular.copy(response.data, services.items);
        });
    };

    services.add = function(hr) {
        var dfd = $q.defer();

        Upload.upload({
            url: '/humanResource/addHumanResource',
            data: {
                humanR: hr,
                file: {
                    image_file: hr.file_image,
                    doc_birth_certificate: hr.doc_birth_certificate,
                    doc_ine: hr.doc_ine,
                    doc_curp: hr.doc_curp,
                    doc_proof_of_address: hr.doc_proof_of_address,
                    doc_rfc: hr.doc_rfc,
                    doc_curricula: hr.doc_curricula,
                    doc_proof_of_studies: hr.doc_proof_of_studies,
                    doc_withholding_notice_infonavit: hr.doc_withholding_notice_infonavit,
                    doc_social_security_number: hr.doc_social_security_number,
                    doc_driver_license: hr.doc_driver_license,
                    doc_recomendation_letter: hr.doc_recomendation_letter,
                    doc_medical_certificate: hr.doc_medical_certificate,
                    doc_medical_history: hr.doc_medical_history,
                    doc_antidoping_test: hr.doc_antidoping_test
                }
            }
        }).then(function(response) {
            if (response) {
                console.log(response.data);
                services.items.push(response.data);
                dfd.resolve(true);
            } else {
                dfd.resolve(false);
            }
        }, function(resp) {
            console.log('Error status: ' + resp.status);
        });
        return dfd.promise;
    };

    services.update = function(hr, index) {
        console.log(hr, index);
        var dfd = $q.defer();

        Upload.upload({
            url: '/humanResource/editHumanResource',
            data: {
                human: hr,
                file: {
                    image_file: hr.file_image,
                    doc_birth_certificate: hr.doc_birth_certificate,
                    doc_ine: hr.doc_ine,
                    doc_curp: hr.doc_curp,
                    doc_proof_of_address: hr.doc_proof_of_address,
                    doc_rfc: hr.doc_rfc,
                    doc_curricula: hr.doc_curricula,
                    doc_proof_of_studies: hr.doc_proof_of_studies,
                    doc_withholding_notice_infonavit: hr.doc_withholding_notice_infonavit,
                    doc_social_security_number: hr.doc_social_security_number,
                    doc_driver_license: hr.doc_driver_license,
                    doc_recomendation_letter: hr.doc_recomendation_letter,
                    doc_medical_certificate: hr.doc_medical_certificate,
                    doc_medical_history: hr.doc_medical_history,
                    doc_antidoping_test: hr.doc_antidoping_test
                }
            }

        }).then(function(response) {
            if (response) {
                console.log(response.data);
                services.items[index] = response.data;
                dfd.resolve(true);
            } else {
                dfd.resolve(false);
            }
        }, function(resp) {
            console.log('Error status: ' + resp.status);
        });
        return dfd.promise;

        /**
         * services.items[index] = hr;
        return services;
         */

    };

    services.delete = function(hr, desc) {
        var obj = {};
        obj.humanResource = hr;
        obj.description = desc;

        var dfd = $q.defer();

        $http.post('/humanResource/deleteHumanResource', obj).then(function(response) {
            if (response) {
                var index;
                index = services.items.indexOf(hr);
                services.items.splice(index, 1);
                dfd.resolve(true);
            } else {
                dfd.resolve(false);
            }
        });
        return dfd.promise;
    };

    services.indexOf = function(hr) {
        return services.items.indexOf(hr);
    };

    return services;
});