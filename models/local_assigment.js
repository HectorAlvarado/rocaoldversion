'use strict';

var mongoose = require('mongoose');

var localAssigmentSchema = mongoose.Schema({
    name: {
        type: String
    },
    id_emitter: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }],
    id_receptor: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }],
    id_asset: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Asset'
    }],
    id_location: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Location'
    }],
    created_date: {
        type: String,
        default: Date.now
    },
    doc_asset: {
        type: String
    },
    accepted: {
        type: Boolean,
        default: false
    }
});
module.exports = mongoose.model("LocalAssigment", localAssigmentSchema);