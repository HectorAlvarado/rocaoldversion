'use strict';

var mongoose = require('mongoose');

var leaseSchema = mongoose.Schema({
    partial_price: {
        type: Number
    },
    time_laps: {
        type: Number
    },
    init_date: {
        type: String
    },
    final_date: {
        type: Date
    },
    description: {
        type: String
    },
    provider_id: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Client'
    }],
    created_date: {
        type: Date,
        default: Date.now
    },
    payment_type: {
        type: String
    },
    asset_id: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Asset"
    }]
});
module.exports = mongoose.model("Lease", leaseSchema);