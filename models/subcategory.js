'use strict';

var mongoose = require('mongoose');

var subcategorySchema = mongoose.Schema({
    name: {
        type: String
    },
    category: {
        type: String
    }
});
module.exports = mongoose.model("Subcategory", subcategorySchema);