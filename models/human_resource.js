'use strict';

var mongoose = require('mongoose');

var humanResourceSchema = mongoose.Schema({
    name: {
        type: String
    },
    department: {
        type: String
    },
    curp: {
        type: String
    },
    social_security_number: {
        type: String
    },
    phone: {
        type: Number
    },
    email: {
        type: String
    },
    birthday: {
        type: String
    },
    nationality: {
        type: String
    },
    marital_status: {
        type: Boolean
    },
    description: {
        type: String
    },
    url_image: {
        type: String
    },
    address: {
        type: String
    },
    gender: {
        type: String
    },
    rfc: {
        type: String
    },
    location_id: {
        type: String
    },
    final_date: {
        type: Date
    },
    status: {
        type: String,
        default: 'contratado'
    },
    doc_birth_certificate: {
        type: String
    },
    doc_ine: {
        type: String
    },
    doc_curp: {
        type: String
    },
    doc_proof_of_address: {
        type: String
    },
    doc_rfc: {
        type: String
    },
    doc_curricula: {
        type: String
    },
    doc_proof_of_studies: {
        type: String
    },
    doc_withholding_notice_infonavit: {
        type: String
    },
    doc_social_security_number: {
        type: String
    },
    doc_driver_license: {
        type: String
    },
    doc_recomendation_letter: {
        type: String
    },
    doc_labor_recomendation_letter: {
        type: String
    },
    doc_letter_of_no_criminal_record: {
        type: String
    },
    doc_medical_certificate: {
        type: String
    },
    doc_medical_history: {
        type: String
    },
    doc_antidoping_test: {
        type: String
    }
});
module.exports = mongoose.model("HumanResource", humanResourceSchema);