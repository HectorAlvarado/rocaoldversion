'use strict';

var mongoose = require('mongoose');

var assetHistorySchema = mongoose.Schema({
    id_asset: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Asset'
    }],
    id_assigment: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Assigment'
    }]
});
module.exports = mongoose.model("AssetHistory", assetHistorySchema);