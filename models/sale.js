'use strict';

var mongoose = require('mongoose');

var saleSchema = mongoose.Schema({
    paymetn_type: {
        type: String
    },
    total_price: {
        type: Number
    },
    partial_payment: {
        type: Number
    },
    created_date: {
        type: String
    },
    lapse: {
        type: String
    },
    no_payment: {
        type: String
    },
    final_date: {
        type: String
    },
    client: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Client'
    }],
    asset_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Asset'
    },
    description: {
        type: String
    }
});
module.exports = mongoose.model("Sale", saleSchema);