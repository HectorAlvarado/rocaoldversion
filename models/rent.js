'use strict';

var mongoose = require('mongoose');

var rentSchema = mongoose.Schema({

    partial_price: {
        type: Number
    },
    time_laps: {
        type: Number
    },
    init_date: {
        type: String
    },
    final_date: {
        type: String
    },
    description: {
        type: String
    },
    client_id: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Client'
    }],
    created_date: {
        type: Date,
        default: Date.now
    },
    payment_type: {
        type: String
    },
    asset_id: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Asset"
    }]
});
module.exports = mongoose.model("Rent", rentSchema);