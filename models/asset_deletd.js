'use strict';

var mongoose = require('mongoose');

var assetDeletedSchema = mongoose.Schema({

    name: {
        type: String
    },
    category: {
        type: String
    },
    subcategory: {
        type: String
    },
    description: {
        type: String
    },
    brand: {
        type: String
    },
    model: {
        type: String
    },
    year: {
        type: Date
    },
    serial_number: {
        type: String
    },
    created_date: {
        type: Date,
        default: Date.now
    },
    working_status: {
        type: String,
        default: 'almacen'
    },
    possession_status: {
        type: String,
        default: 'own'
    },
    url_image_one: {
        type: String
    },
    url_image_two: {
        type: String
    },
    url_image_three: {
        type: String
    },
    url_image_four: {
        type: String
    },
    doc_guard_asset: {
        type: String
    },
    doc_bill_asset: {
        type: String
    },
    doc_insurance_asset: {
        type: String
    },
    doc_motion_asset: {
        type: String
    },
    final_date: {
        type: Date
    },
    price_value: {
        type: String
    }
});
module.exports = mongoose.model("AssetDeleted", assetDeletedSchema);