'use strict';

var mongoose = require('mongoose');

var assetSchema = mongoose.Schema({
    name: {
        type: String
    },
    category: {
        type: String
    },
    subcategory: {
        type: String
    },
    description: {
        type: String
    },
    brand: {
        type: String
    },
    model: {
        type: String
    },
    year: {
        type: String
    },
    serial_number: {
        type: String
    },
    engine_assets_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'EngineAsset'
    },
    created_date: {
        type: String
    },
    working_status: {
        type: String
    },
    possession_status: {
        type: String
    },
    url_image_one: {
        type: String
    },
    url_image_two: {
        type: String
    },
    url_image_three: {
        type: String
    },
    url_image_four: {
        type: String
    },
    doc_guard_asset: {
        type: String
    },
    doc_bill_asset: {
        type: String
    },
    doc_insurance_asset: {
        type: String
    },
    doc_motion_asset: {
        type: String
    },
    final_date: {
        type: String
    },
    location_id: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Location'
    }],
    price_value: {
        type: String
    }
});
module.exports = mongoose.model("Asset", assetSchema);