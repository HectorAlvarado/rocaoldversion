'use strict';

var mongoose = require('mongoose');

var humanResourcesHistory = mongoose.Schema({
    human_resources_id: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'HumanResource'
    }],
    description: {
        type: String
    },
    created_date: {
        type: Date,
        default: Date.now
    }
});
module.exports = mongoose.model("HumanResourceHistory", humanResourcesHistory);