'use strict';

var mongoose = require('mongoose');

var notificationSchema = mongoose.Schema({
    subject: {
        type: String
    },
    created_date: {
        type: String,
        default: ''
    },
    content: {
        type: String
    },
    viewed: {
        type: Boolean,
        default: false
    },
    id_user: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }]
});

module.exports = mongoose.model("Notification", notificationSchema);