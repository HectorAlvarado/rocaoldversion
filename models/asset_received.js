'use strict';

var mongoose = require('mongoose');

var assetReceivedSchema = mongoose.Schema({
    id_asset: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Asset'
    }],
    id_assigment: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Assigment'
    }],
    id_receptor: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }]
});
module.exports = mongoose.model("AssetReceived", assetReceivedSchema);