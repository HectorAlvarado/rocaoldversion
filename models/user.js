'use strict';

var mongoose = require('mongoose');
var encrypt = require('../utilities/encryption');

var userSchema = mongoose.Schema({

    email: {
        type: String,
        required: true,
        unique: true
    },
    salt: {
        type: String,
        required: '{PATH} is required!'
    },
    hashed_pwd: {
        type: String,
        required: '{PATH} is required!'
    },
    created_date: {
        type: Date,
        default: Date.now
    },
    notifications: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Notification',
        default: ''
    }],
    id_human_resources: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'HumanResource',
        default: ''
    }],
    role: {
        type: String
    }

});

userSchema.methods = {
    authenticate: function(passwordToMach) {
        return encrypt.hashPwd(this.salt, passwordToMach) === this.hashed_pwd;
    }
};
/*
var Users = mongoose.model('User', userSchema);

Users.find({}).exec(function(err, collection) {
    if (collection.length === 0) {
        var salt, hash;

        salt = encrypt.createSalt();
        hash = encrypt.hashPwd(salt, 'balio');

        Users.create({
            email: 'balio@balio.com',
            salt: salt,
            hashed_pwd: hash,
        });
    }
});

      salt = encrypt.createSalt();
      hash = encrypt.hashPwd(salt, 'hector');

      Users.create({
         user_name: 'Hector',
         username: 'Hector',
         salt: salt,
         roles: ['sales'],
         hashed_pwd: hash,
      });

      salt = encrypt.createSalt();
      hash = encrypt.hashPwd(salt, 'seth');

      Users.create({
         user_name: 'Seth',
         username: 'Seth',
         salt: salt,
         roles: [],
         hashed_pwd: hash,
      });
   }
});
*/
module.exports = mongoose.model("User", userSchema);