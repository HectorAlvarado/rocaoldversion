'use strict';

var mongoose = require('mongoose');

var locationSchema = mongoose.Schema({
    name: {
        type: String
    },
    address: {
        type: String
    },
    lat: {
        type: String
    },
    lon: {
        type: String
    },
    cadastral_key: {
        type: String
    },
    status: {
        type: String
    },
    created_date: {
        type: Date,
        default: Date.now
    },
    final_date: {
        type: Date
    },
    residents: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'HumanResource'
    }],
    location_type: {
        type: String
    }
});
module.exports = mongoose.model("Location", locationSchema);