'use strict';

var mongoose = require('mongoose');

var engineAssetSchema = mongoose.Schema({
    id_asset: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Asset"
    },
    fuel_type: {
        type: String
    },
    fuel_tank_size: {
        type: String
    },
    plates: {
        type: String
    }
});
module.exports = mongoose.model("EngineAsset", engineAssetSchema);