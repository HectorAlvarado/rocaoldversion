'use strict';

var mongoose = require('mongoose');

var roleShcema = mongoose.Schema({
    name: {
        type: String
    },

    permission: {
        type: String
    }
});
module.exports = mongoose.model("Role", roleShcema);