'use strict';

var mongoose = require('mongoose');

var userWorkingDaySchema = mongoose.Schema({
    id_human_resource: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'HumanResource'
    }],
    created_date: {
        type: Date,
        default: Date.now
    },
    in_hour: {
        type: Date
    },
    out_hour: {
        type: Date
    }
});
module.exports = mongoose.model("UserWorkingDay", userWorkingDaySchema);