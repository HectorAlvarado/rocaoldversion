'use strict';

var mongoose = require('mongoose');

var clientSchema = mongoose.Schema({
    name: {
        type: String
    },
    fiscal_name: {
        type: String
    },
    address: {
        type: String
    },
    contact_name_1: {
        type: String
    },
    contact_mail_1: {
        type: String
    },
    contact_phone_1: {
        type: Number
    },
    contact_name_2: {
        type: String
    },
    contact_mail_2: {
        type: String
    },
    contact_phone_2: {
        type: Number
    },
    contact_name_3: {
        type: String
    },
    contact_mail_3: {
        type: String
    },
    contact_phone_3: {
        type: Number
    },
    rfc: {
        type: String
    },
    zip: {
        type: Number
    },
    category: {
        type: String
    },
    final_date: {
        type: Date
    }
});
module.exports = mongoose.model("Client", clientSchema);