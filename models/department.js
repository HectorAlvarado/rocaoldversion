'use strict';

var mongoose = require('mongoose');

var departmentSchema = mongoose.Schema({
    name: {
        type: String
    }
});
module.exports = mongoose.model("Department", departmentSchema);