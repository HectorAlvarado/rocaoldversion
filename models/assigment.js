'use strict';

var mongoose = require('mongoose');

var assigmentSchema = mongoose.Schema({
    name: {
        type: String
    },
    id_emitter: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }],
    id_receptor: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }],
    assets: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Asset'
    }],
    id_location: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Location'
    }],
    created_date: {
        type: Date,
        default: Date.now
    },
    accepted_date: {
        type: Date
    },
    accepted: {
        type: Boolean,
        default: false
    },
    comments: {
        type: String
    }
});
module.exports = mongoose.model("Assigment", assigmentSchema);