'use strict';

var mongoose = require('mongoose');
var Notification = mongoose.model('Notification');
var User = mongoose.model('User');

exports.createNotification = function(req, res) {
    var new_notification = new Notification();
    new_notification.subject = req.body.subject;
    new_notification.content = req.body.content;
    new_notification.id_user = req.body.id_user;

    new_notification.save(function(err, saved_notification) {
        if (err) {
            console.log(err);
            return res.status(500).send();
        } else {
            return res.status(200).send();
        }
    });
};

exports.updateNotifications = function(req, res) {
    var user_id = req.body.userInfo;
    var notifications_to_update = req.body.notifications_to_update;
    var i = 0;

     for (var notification_update in notifications_to_update) {
        Notification.findOneAndUpdate({ _id: notifications_to_update[notification_update] }, 
            {$set:{ viewed: true }}, 
            { new: true })
            .exec(function(err, notification_updated) {
                if (err) {
                    console.log('ERROR');
                } else {
                    i++;
                    if (i === notifications_to_update.length) {
                        User.findOne({ _id : user_id })
                        .populate('notifications')
                        .exec(function(err, user) {
                            if (err) {
                                console.log('ERROR');
                            } else {
                                res.send(user);
                            }
                        });
                    } 
                }
         });
    }
};

exports.deleteNotifications = function(req, res) {
    var user_id = req.body.userInfo._id;
    var notifications = req.body.userInfo.notifications;
    var notification_to_delete = req.body.notification_to_delete;

    Notification.findByIdAndRemove(notification_to_delete, function (err, notification) {
        if (err) {
            console.log(err);   
        } else {
            User.findOneAndUpdate({ _id: user_id }, 
            {$set:{ notifications : notifications }}, {new: true} )
            .populate('notifications')
            .exec(function(err, user) {
                if (err) { 
                    console.log(err);
                } else {
                    res.send(user);
                }
            });
        }
    });
};