'use strict';

var mongoose = require('mongoose');
var Client = mongoose.model('Client');
var Q = require('q');

exports.getAllClients = function(req, res) {

    Client.find().then(function(clients) {
        if (!clients) {
            console.log('error from getting clients in server: ../controllers/clients');
        } else {
            console.log('getting clients in server: ../controllers/clients');
            res.json(clients);
        }
    });
};

exports.addClient = function(req, res) {

    var client = req.body.newClient;

    Client.create(client).then(function(nClient) {
        if (nClient) {
            console.log('client has been created in the server: ../controllers/clients');
            res.send(nClient);
        } else {
            console.log('error saveing the client in server: ../controllers/clients');
            res.send('error');
        }
    });
};

exports.editClient = function(req, res) {
    var clientToEdit = req.body;

    var street = clientToEdit.editClient.address_parts.street;
    var suburb = clientToEdit.editClient.address_parts.suburb;
    var state = clientToEdit.editClient.address_parts.state;

    Client.findById(clientToEdit.editClient._id).then(function(client) {
        if (client) {
            client.name = clientToEdit.editClient.name;
            client.rfc = clientToEdit.editClient.rfc;
            client.zip = clientToEdit.editClient.zip;
            client.category = clientToEdit.editClient.category;
            client.fiscal_name = clientToEdit.editClient.fiscal_name;
            client.address = street + ', ' + suburb + ', ' + state;
            client.contact_name_1 = clientToEdit.editClient.contact_name_1;
            client.contact_mail_1 = clientToEdit.editClient.contact_mail_1;
            client.contact_phone_1 = clientToEdit.editClient.contact_phone_1;
            client.contact_mail_2 = clientToEdit.editClient.contact_mail_2;
            client.contact_name_2 = clientToEdit.editClient.contact_name_2;
            client.contact_phone_2 = clientToEdit.editClient.contact_phone_2;
            client.contact_name_3 = clientToEdit.editClient.contact_name_3;
            client.contact_mail_3 = clientToEdit.editClient.contact_mail_3;
            client.contact_phone_3 = clientToEdit.editClient.contact_phone_3;

            client.save(function(err) {
                if (!err) {
                    res.send(client);
                    console.log('client has been edited in server: ../controllers/clients');
                } else {
                    console.log('error has ocurre with editing client in server: ../controllers/clients');
                    console.log(err);
                }
            });
        } else {
            console.log('error editing the client in server: ../controllers/clients');
            res.send('error');
        }
    });
};

exports.eliminateClient = function(req, res) {
    var client = req.body;
    var clientId = client.clientToEliminate._id;

    Client.findById(clientId).then(function(clientToEliminate) {
        if (clientToEliminate) {
            console.log('client has been eliminated in server: ../controllers/clients');
            clientToEliminate.remove();
            res.send(clientToEliminate);
        } else {
            console.log('error eliminating client in server: ../controllers/clients');
        }
    });
};