'use strict';

var mongoose = require('mongoose');
var Role = mongoose.model('Role');
var User = mongoose.model('User');

exports.getAllRoles = function() {
    Role.find(function(err, roles) {
        if (err) {
            console.log(err);
            return res.status(500).send();
        } else {
            return res.status(200).send(roles);
        }
    });
};

exports.createRole = function(req, res) {
    var new_role = new Role();
    new_role.name = req.body.name;
    new_role.permission = req.body.permission;

    new_role.save(function(err, saved_role) {
        if (err) {
            console.log(err);
            return res.status(500).send();
        } else {
            return res.status(200).send();
        }
    });
};

exports.updateRole = function(req, res) {
    var new_role_name = req.body.info.new_role_name;
    var role_id = req.body.info.role_id;
    var new_role = req.body.info.role;

    Role.findOne({ '_id' : role_id }, function (err, role){
        if(err){
            console.log(err);
            return res.status(500).send();
        } else {
            role.name = new_role_name;
            role.permission = new_role;
            role.save(function(err, updated_role) {
                if(err){
                    console.log(err);
                    return res.status(500).send();
                } else {
                    User.find(function(err, users) {
                        if (!err) {
                            for(var user in users) {
                                var current_user = users[user];
                                //var stringify = JSON.stringify({"name":"rrrr","permissions":"password"});
                                var user_role = JSON.parse(current_user.role);
                                if(user_role.name === new_role_name){
                                    User.findOne({ _id: current_user._id }, function (err, user){
                                        user.role = new_role;
                                        user.save();
                                    });
                                }
                            }
                        } else {
                            console.log(err);
                        }
                    });
                }
            });
        }
    });
};

/*exports.deleteRole = function(req, res) {
    Role.findByIdAndRemove(req.body.id, function (err, role) {
        if (err) {
            console.log(err);   
            return res.status(500).send();
        } else {
            return res.status(200).send();
        }
    })
};*/