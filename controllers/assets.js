'use strict';

var mongoose = require('mongoose');
var Asset = mongoose.model('Asset');
var EngineAsset = mongoose.model('EngineAsset');
var RentAsset = mongoose.model('Rent');
var SaleAsset = mongoose.model('Sale');
var LeaseAsset = mongoose.model('Lease');

var gcloud = require('google-cloud')({
    projectId: 'roca-144118',
    credentials: require('../roca-a95f6479fc8f.json')
});
var gcs = gcloud.storage();
var bucket = gcs.bucket('rocka-bucket');

exports.getAllAssets = function(req, res) {
    Asset.find().populate('engine_assets_id').then(function(assets) {
        if (assets) {
            console.log('getting all assets is working in: ../controllers/assets');
            //console.log(assets);
            res.json(assets);
        } else {
            console.log('there is been an error getting all assets in: ../controllers/assets');
            res.status(400);
        }
    });
};

exports.addAsset = function(req, res) {

    var asset = req.body.newAsset;
    var engine_asset = req.body.newAsset.engine_assets_id;
    var file = req.files.file;
    var docs = Object(file);
    var i = 0;
    var numberOfImgs = Object.keys(docs).length;
    console.log(asset);


    var url_base = 'https://storage.googleapis.com/rocka-bucket/';

    var fakeId = mongoose.Types.ObjectId('4edd40c86762e0fb12000003');
    asset.engine_assets_id = fakeId;

    Asset.create(asset).then(function(newAsset) {
        if (newAsset) {
            if (file) {
                for (var f in file) {
                    var key = docs[f].fieldName.match(/\[(.*?)\]/);
                    var fileType = docs[f].originalFilename.split('.');

                    newAsset[key[1]] = url_base + 'assets/' + newAsset._id + '/' + key[1] + '.' + fileType[1];

                    bucket.upload(docs[f].path, {
                        destination: bucket.file(
                            'assets/' + newAsset._id + '/' + key[1] + '.' + fileType[1]
                        )
                    }, function(err, file) {
                        if (!err) {
                            console.log('img is now in ROCA bucket (assets)');
                            i++;
                            if (numberOfImgs === i) {
                                if (asset.engine_assets_id.fuel_type !== '' && asset.engine_assets_id.fuel_tank_size !== '') {
                                    var nea = new EngineAsset();
                                    var na = new Asset();

                                    na = newAsset;
                                    na.engine_assets_id = nea._id;
                                    na.save(function(err) {
                                        if (!err) {
                                            nea.fuel_type = engine_asset.fuel_type;
                                            nea.fuel_tank_size = engine_asset.fuel_tank_size;
                                            nea.plates = engine_asset.plates;

                                            nea.save(function(err) {
                                                if (!err) {
                                                    res.send(na);
                                                    console.log('creating the new asset with an engine asset record and img in google cloud: ../controllers/assets ');
                                                } else {
                                                    console.log('problem creating the new asset with an engine asset record and img in google cloud: ../controllers/assets ');
                                                    console.log(err);
                                                }
                                            });
                                        }
                                    });
                                }
                            }
                        }
                    });
                }
            } else {
                if (!file && asset.engine_assets_id.fuel_type === undefined && asset.engine_assets_id.fuel_tank_size === undefined && asset.engine_assets_id.plates === undefined) {
                    var nea = new EngineAsset();
                    var na = new Asset();

                    na = newAsset;
                    na.engine_assets_id = nea._id;
                    na.save(function(err) {
                        if (!err) {
                            nea.fuel_type = engine_asset.fuel_type;
                            nea.fuel_tank_size = engine_asset.fuel_tank_size;
                            nea.plates = engine_asset.plates;

                            nea.save(function(err) {
                                if (!err) {
                                    res.send(na);
                                    console.log('creating the new asset with an engine asset record: ../controllers/assets');
                                } else {
                                    console.log('problem creating the new asset with an engine asset record: ../controllers/assets');
                                    console.log(err);
                                }
                            });
                        }
                    });
                } else {
                    console.log('creating the new asset with out an engine asset record: ../controllers/assets');
                    res.send(newAsset);
                }
            }
        } else {
            console.log('problem creating the new asset: ../controllers/assets');
        }
    });
};

exports.editAsset = function(req, res) {

    var asset = req.body.newAsset;
    var engine_asset = req.body.newAsset.engine_assets_id;
    var file = req.files.file;
    var docs = Object(file);
    var i = 0;
    var j = 0;
    var numberOfImgs = Object.keys(docs).length;

    var url_base = 'https://storage.googleapis.com/rocka-bucket/';

    var fakeId = mongoose.Types.ObjectId('4edd40c86762e0fb12000003');
    asset.engine_assets_id = fakeId;

    Asset.findById(asset._id).then(function(assetToEdit) {
        if (assetToEdit) {
            if (file) {
                for (var f in file) {
                    var key = docs[f].fieldName.match(/\[(.*?)\]/);
                    var fileType = docs[f].originalFilename.split('.');

                    assetToEdit[key[1]] = url_base + 'assets/' + assetToEdit._id + '/' + key[1] + '.' + fileType[1];

                    var imgToSplit = assetToEdit[key[1]];
                    var imgToDelete = imgToSplit.split('rocka-bucket/')[1];

                    var img_file = bucket.file(imgToDelete);
                    //console.log(img_file);
                    img_file.delete(function(err, apiREsponse) {
                        if (!err) {
                            console.log('img from ROCA bucket (assets) is gone');
                            j++;
                            if (numberOfImgs === j) {
                                bucket.upload(docs[f].path, {
                                    destination: bucket.file(
                                        'assets/' + assetToEdit._id + '/' + key[1] + '.' + fileType[1]
                                    )
                                }, function(err, file) {
                                    if (!err) {
                                        i++;
                                        if (numberOfImgs === i) {
                                            if (engine_asset.plates !== undefined && engine_asset.fuel_type !== undefined && engine_asset.fuel_tank_size !== undefined) {
                                                console.log('asset to edit has been found in: ../controllers/assets');
                                                assetToEdit.name = asset.name;
                                                assetToEdit.category = asset.category;
                                                assetToEdit.subcategory = asset.subcategory;
                                                assetToEdit.description = asset.description;
                                                assetToEdit.brand = asset.brand;
                                                assetToEdit.model = asset.model;
                                                assetToEdit.year = asset.year;
                                                assetToEdit.serial_number = asset.serial_number;
                                                assetToEdit.created_date = asset.created_date;
                                                assetToEdit.possession_status = asset.possession_status;
                                                assetToEdit.working_status = asset.working_status;

                                                assetToEdit.save(function(err) {
                                                    if (!err) {
                                                        console.log('Asset has been modificated and send back to client in: ../controllers/assets');
                                                        res.send(assetToEdit);
                                                        EngineAsset.findById(engine_asset._id).then(function(engineToEdit) {
                                                            if (engineToEdit) {
                                                                engineToEdit.fuel_type = engine_asset.fuel_type;
                                                                engineToEdit.fuel_tank_size = engine_asset.fuel_tank_size;
                                                                engineToEdit.plates = engine_asset.plates;

                                                                engineToEdit.save();
                                                                console.log('engine to edit has change in ../controllers/assets');
                                                            } else {
                                                                console.log('engine can not bee changed in ../controllers/assets');
                                                            }
                                                        });
                                                    } else {
                                                        console.log('Error modificating the Asset');
                                                        console.log(err);
                                                    }
                                                });
                                            } else if (engine_asset.plates === undefined && engine_asset.fuel_type === undefined && engine_asset.fuel_tank_size === undefined) {
                                                console.log('asset to edit has been found in: ../controllers/assets');
                                                assetToEdit.name = asset.name;
                                                assetToEdit.category = asset.category;
                                                assetToEdit.subcategory = asset.subcategory;
                                                assetToEdit.description = asset.description;
                                                assetToEdit.brand = asset.brand;
                                                assetToEdit.model = asset.model;
                                                assetToEdit.year = asset.year;
                                                assetToEdit.serial_number = asset.serial_number;
                                                assetToEdit.created_date = asset.created_date;
                                                assetToEdit.possession_status = asset.possession_status;
                                                assetToEdit.working_status = asset.working_status;

                                                assetToEdit.save(function(err) {
                                                    if (!err) {
                                                        console.log('Asset has been modify in: ../controllers/assets');
                                                        res.send(assetToEdit);
                                                    }
                                                });
                                            }
                                        }
                                        console.log('img to edit is now in ROCA bucket (assets)');
                                    } else {
                                        console.log('there is a problem editin img and uploading it to bucket (assets)');
                                    }
                                });
                            }
                        } else {
                            console.log('problem in editting with file in assets: ../controllers/assets');
                        }
                    });
                }
            }
            if (!file) {
                if (engine_asset.plates !== undefined && engine_asset.fuel_type !== undefined && engine_asset.fuel_tank_size !== undefined) {
                    console.log('asset to edit has been found with NO file in: ../controllers/assets');
                    assetToEdit.name = asset.name;
                    assetToEdit.category = asset.category;
                    assetToEdit.subcategory = asset.subcategory;
                    assetToEdit.description = asset.description;
                    assetToEdit.brand = asset.brand;
                    assetToEdit.model = asset.model;
                    assetToEdit.year = asset.year;
                    assetToEdit.serial_number = asset.serial_number;
                    assetToEdit.created_date = asset.created_date;
                    assetToEdit.possession_status = asset.possession_status;
                    assetToEdit.working_status = asset.working_status;

                    assetToEdit.save(function(err) {
                        if (!err) {
                            console.log('Asset has been modificated with NO file in: ../controllers/assets');
                            EngineAsset.findById(engine_asset._id).then(function(engineToEdit) {
                                if (engineToEdit) {
                                    engineToEdit.fuel_type = engine_asset.fuel_type;
                                    engineToEdit.fuel_tank_size = engine_asset.fuel_tank_size;
                                    engineToEdit.plates = engine_asset.plates;
                                    engineToEdit.save(function(err) {
                                        if (!err) {
                                            res.send(assetToEdit);
                                            console.log('engine to edit has change with NO file in: ../controllers/assets');
                                        } else {
                                            console.log('error making the change on asset and engine asset with NO file in: ../controllers/assets');
                                        }
                                    });
                                } else {
                                    console.log('engines can not bee changed with NO file in: ../controllers/assets');
                                    console.log(err);
                                }
                            });
                        } else {
                            console.log('Error modificating the Asset with NO file in: ../controllers/assets');
                            console.log(err);
                        }
                    });
                } else if (engine_asset.plates === undefined && engine_asset.fuel_type === undefined && engine_asset.fuel_tank_size === undefined) {
                    console.log('asset to edit has been found in with NO file and NO engine asset: ../controllers/assets');
                    assetToEdit.name = asset.name;
                    assetToEdit.category = asset.category;
                    assetToEdit.subcategory = asset.subcategory;
                    assetToEdit.description = asset.description;
                    assetToEdit.brand = asset.brand;
                    assetToEdit.model = asset.model;
                    assetToEdit.year = asset.year;
                    assetToEdit.serial_number = asset.serial_number;
                    assetToEdit.created_date = asset.created_date;
                    assetToEdit.possession_status = asset.possession_status;
                    assetToEdit.working_status = asset.working_status;

                    assetToEdit.save(function(err) {
                        if (!err) {
                            console.log('Asset has been modificated in: ../controllers/assets');
                            res.send(assetToEdit);
                        } else {
                            console.log('problem editing the asset with our the engine asset in: ../controllers/assets');
                            console.log(err);
                        }
                    });
                }
            }
        } else {
            console.log('there is a problem finding the new asset with NO file to edit in: ../controllers/assets');
        }
    });
};

exports.rentAsset = function(req, res) {

    var assetToRent = req.body.asset;
    var rent_info = req.body.rent_info;

    var fakeId = mongoose.Types.ObjectId('4edd40c86762e0fb12000003');

    var newAssetToRent = new RentAsset();

    newAssetToRent.asset_id.push(assetToRent._id);
    newAssetToRent.client_id.push(fakeId);
    newAssetToRent.partial_price = rent_info.price;
    newAssetToRent.time_laps = rent_info.lapse;
    newAssetToRent.init_date = rent_info.created_date;
    newAssetToRent.description = rent_info.description;

    newAssetToRent.save(function(err) {
        if (!err) {
            console.log('new collection rent has been created in: ../controllers/assets');
        } else {
            console.log('error creating the new rent colection in: ../controllers/assets');
            console.log(err);
        }
    });

    Asset.findById(assetToRent._id).then(function(assetInCollection) {
        assetInCollection.possession_status = assetToRent.possession_status;
        assetInCollection.save(function(err) {
            if (!err) {
                console.log('possesion status of asset in collection has been edited to rent in: ../controllers/assets');
                res.send(assetInCollection);
            } else {
                console.log('error trying change the possesion status to not rent in ../controllers/assets');
                console.log(err);
            }
        });
    });
};

exports.finishRentAsset = function(req, res) {
    var dataToFinishRent = req.body.rent_data;

    Asset.findById(dataToFinishRent._id).then(function(assetInCollection) {
        assetInCollection.possession_status = dataToFinishRent.possession_status;
        assetInCollection.save(function(err) {
            if (!err) {
                console.log('possesion status of asset in collection has been edited to not rent in: ../controllers/assets');
                res.send(assetInCollection);
            } else {
                console.log('error trying change the possesion status to not rent in ../controllers/assets');
                console.log(err);
            }
        });
    });
};

exports.sellAsset = function(req, res) {
    var assetToSell = req.body.assetToSell;
    var currentAsset = req.body.current_asset;

    var fakeId = mongoose.Types.ObjectId('4edd40c86762e0fb12000003');

    var assetToBeOnSale = new SaleAsset();
    assetToBeOnSale.paymetn_type = assetToSell.paymetn_type;
    assetToBeOnSale.total_price = assetToSell.total_price;
    assetToBeOnSale.partial_payment = assetToSell.partial_payment;
    assetToBeOnSale.created_date = assetToSell.created_date;
    assetToBeOnSale.lapse = assetToSell.lapse;
    assetToBeOnSale.no_payment = assetToSell.no_payment;
    assetToBeOnSale.final_date = assetToSell.final_date;
    assetToBeOnSale.client.push(fakeId);
    assetToBeOnSale.asset_id = currentAsset._id;
    assetToBeOnSale.description = assetToSell.description;

    assetToBeOnSale.save(function(err) {
        if (!err) {
            console.log('A sale has been made and the record has been saved');
            Asset.findById(currentAsset._id).then(function(sellingThisAsset) {
                sellingThisAsset.remove();
                res.send(sellingThisAsset);
            });
        } else {
            console.log('error trying sale the asset in ../controllers/assets');
        }
    });
};

exports.leaseAsset = function(req, res) {
    var assetToLease = req.body.rent_lease;
    var currentAsset = req.body.current_asset;

    var fakeId = mongoose.Types.ObjectId('4edd40c86762e0fb12000003');

    var newAssetToLease = new LeaseAsset();

    newAssetToLease.asset_id.push(currentAsset._id);
    newAssetToLease.provider_id.push(fakeId);
    newAssetToLease.partial_price = assetToLease.price;
    newAssetToLease.time_laps = assetToLease.lapse;
    newAssetToLease.init_date = assetToLease.created_date;
    newAssetToLease.description = assetToLease.description;

    newAssetToLease.save(function(err) {
        if (!err) {
            console.log('new collection lease has been created in: ../controllers/assets');
        } else {
            console.log('error creating the new lease colection in: ../controllers/assets');
            console.log(err);
        }
    });

    Asset.findById(currentAsset._id).then(function(assetInCollection) {
        assetInCollection.possession_status = 'arrendado';
        assetInCollection.save(function(err) {
            if (!err) {
                console.log('possesion status of asset in collection has been edited to lease in: ../controllers/assets');
                res.send(assetInCollection);
            } else {
                console.log('error trying change the possesion status to lease in ../controllers/assets');
                console.log(err);
            }
        });
    });
};