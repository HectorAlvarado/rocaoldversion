'use strict';

var mongoose = require('mongoose');
var encrypt = require('../utilities/encryption');

var User = mongoose.model('User');
var Human_resource = mongoose.model('HumanResource');

exports.getAllUsers = function(req, res) {
    User.find().populate('id_human_resources').exec(function(err, users) {
        if (err) {
            console.log(err);
            return res.send(false);
        } else {
            return res.status(200).send(users);
        }
    });
};

exports.getAllhumanResources = function(req, res) {
    Human_resource.find().select('_id name department email url_image').exec(function(err, humans_resources) {
        if (err) {
            console.log(err);
            return res.send(false);
        } else {
            return res.status(200).send(humans_resources);
        }
    });
};

exports.createUser = function(req, res) {
    var new_user = new User();
    new_user.email = req.body.email;
    new_user.salt = encrypt.createSalt();
    new_user.hashed_pwd = encrypt.hashPwd(new_user.salt, req.body.password);
    new_user.role = req.body.role;
    if (req.body.hr_link) {
        new_user.id_human_resources = req.body.id_hr._id;
    }
    new_user.save(function(err, saved_user) {
        if (err) {
            console.log(err);
            return res.status(500).send();
        } else {
            return res.status(200).send(saved_user);
        }
    });
};

exports.updateUser = function(req, res) {
    var user_info = req.body;
    console.log('INFO QUE LLEGA DEL USER EN UPDATE');
    console.log(user_info);
    User.findOne({ _id: user_info._id }, function(err, user) {
        user.email = user_info.email;
        user.id_human_resources = (req.body.hr_link && req.body.id_human_resources.length === 0) ? req.body.id_hr._id : [];
        user.role = user_info.role;
        if (user_info.new_password && user_info.new_password.length > 0) {
            user.salt = encrypt.createSalt();
            user.hashed_pwd = encrypt.hashPwd(user.salt, user_info.new_password);
        }
        user.save(function(err, updated_user) {
            if (err) {
                console.log(err);
                return res.status(500).send();
            } else {
                updated_user.populate('id_human_resources', function(err, user_populated) {
                    if (err) {
                        console.log(err);
                        return res.status(500).send();
                    } else {
                        return res.status(200).send(user_populated);
                    }
                });
            }
        });
    });
};

exports.deleteUser = function(req, res) {
    User.findByIdAndRemove(req.body._id, function(err, user) {
        if (err) {
            console.log(err);
            return res.status(500).send();
        } else {
            return res.status(200).send(user);
        }
    });
};