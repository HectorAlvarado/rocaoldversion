'use strict';

var mongoose = require('mongoose');
var HumanResource = mongoose.model('HumanResource');
var Human_resources_history = mongoose.model('HumanResourceHistory');

var gcloud = require('google-cloud')({
    projectId: 'roca-144118',
    credentials: require('../roca-a95f6479fc8f.json')
});
var gcs = gcloud.storage();
var bucket = gcs.bucket('rocka-bucket');

exports.getAllHumanResources = function(req, res) {
    HumanResource.find({ status: 'contratado' }).then(function(humans) {
        if (humans) {
            console.log('getting all human resources is working: ../controllers/humanResources');
            res.send(humans);
        } else {
            console.log('there is been an error getting all human resources: ../controllers/humanResources');
            res.status(400);
        }
    });
};

exports.addHumanResource = function(req, res) {
    var hR = req.body.humanR;
    console.log(hR);

    var file = req.files.file;
    var docs = Object(file);
    var i = 0;
    var numberOfImgs = Object.keys(docs).length;
    console.log(numberOfImgs);

    var street = hR.address_parts.street;
    var suburb = hR.address_parts.suburb;
    var state = hR.address_parts.state;

    hR.address = street + ', ' + suburb + ', ' + state;
    hR.status = 'contratado';

    var allDocs = ['file_image', 'doc_birth_certificate', 'doc_ine', 'doc_curp', 'doc_proof_of_address', 'doc_rfc', 'doc_curricula', 'doc_proof_of_studies',
        'doc_withholding_notice_infonavit', 'doc_social_security_number', 'doc_driver_license', 'doc_recomendation_letter',
        'doc_labor_recomendation_letter', 'doc_letter_of_no_criminal_record', 'doc_medical_certificate', 'doc_medical_history', 'doc_antidoping_test'
    ];

    //fill the missing fields in human resource model (images).
    for (var j in allDocs) {
        hR[allDocs[j]] = '';
    }

    var url_base = 'https://storage.googleapis.com/rocka-bucket/';

    HumanResource.create(hR).then(function(newHuman) {
        //console.log(newHuman);
        if (newHuman) {
            for (var f in docs) {
                var key = docs[f].fieldName.match(/\[(.*?)\]/);
                var fileType = docs[f].originalFilename.split('.');

                if (key[1] === 'image_file') {
                    newHuman.url_image = url_base + 'human-resources/' + newHuman._id + '/image_file' + '.' + fileType[1];
                } else {
                    newHuman[key[1]] = url_base + 'human-resources/' + newHuman._id + '/' + key[1] + '.' + fileType[1];
                }
                bucket.upload(docs[f].path, {
                    destination: bucket.file(
                        'human-resources/' + newHuman._id + '/' + key[1] + '.' + fileType[1]
                    )
                }, function(err, file) {
                    if (!err) {
                        console.log('img is now in rocka-bucket... (human-resources)');
                        i++;
                        if (numberOfImgs === i) {
                            newHuman.save(function(err) {
                                if (!err) {
                                    console.log(newHuman);
                                    console.log('Human Resource has been save, success in: ../controllers/humanResources');
                                    res.send(newHuman);
                                } else {
                                    console.log('Human Resource has NOT been save, fail in: ../controllers/humanResources');
                                    res.status(400);
                                }
                            });
                        }
                    } else {
                        console.log('problem uploading img in rocka-bucket... (human-resources)');
                        res.status(400);
                    }
                });
            }
        } else {
            console.log('error creating new Human, fail: ../controllers/humanResources');
            res.status(400);
        }
    });
};

exports.editHumanResource = function(req, res) {
    var humanToEdit = req.body;
    //console.log(humanToEdit);

    var file = req.files.file;
    var docs = Object(file);
    var i = 0;
    var numberOfImgs = Object.keys(docs).length;
    //console.log(docs);

    var street = humanToEdit.human.address_parts.street;
    var suburb = humanToEdit.human.address_parts.suburb;
    var state = humanToEdit.human.address_parts.state;

    humanToEdit.human.address = street + ', ' + suburb + ', ' + state;

    var allDocs = ['file_image', 'doc_birth_certificate', 'doc_ine', 'doc_curp', 'doc_proof_of_address', 'doc_rfc', 'doc_curricula', 'doc_proof_of_studies',
        'doc_withholding_notice_infonavit', 'doc_social_security_number', 'doc_driver_license', 'doc_recomendation_letter',
        'doc_labor_recomendation_letter', 'doc_letter_of_no_criminal_record', 'doc_medical_certificate', 'doc_medical_history', 'doc_antidoping_test'
    ];

    //fill the missing fields in human resource model (images).
    for (var j in allDocs) {
        humanToEdit.human[allDocs[j]] = '';
    }

    var url_base = 'https://storage.googleapis.com/rocka-bucket/';

    HumanResource.findById(humanToEdit.human._id).then(function(humanToChange) {
        if (humanToChange) {
            //console.log(humanToChange);
            for (var f in docs) {
                var key = docs[f].fieldName.match(/\[(.*?)\]/);
                var fileType = docs[f].originalFilename.split('.');

                if (key[1] === 'image_file') {
                    humanToChange.url_image = url_base + 'human-resources/' + humanToChange._id + '/image_file' + '.' + fileType[1];
                } else {
                    humanToChange[key[1]] = url_base + 'human-resources/' + humanToChange._id + '/' + key[1] + '.' + fileType[1];
                }

                var imageFileToDelete = bucket.file('human-resources/' + humanToChange._id + '/image_file' + '.' + fileType[1]);
                var docsFilesToDelete = bucket.file('human-resources/' + humanToChange._id + '/' + key[1] + '.' + fileType[1]);

                if (key[1] === 'image_file') {
                    imageFileToDelete.delete(function(err, responseApi) {
                        if (!err) {
                            console.log('img has been deleted (image_file = url_image) from the cloud: ../controllers/humanResources');
                        } else {
                            console.log("img has NOT been deleted (image_file = url_image) from the cloud: ../controllers/humanResources");
                            console.log(err);
                        }
                    });
                } else {
                    docsFilesToDelete.delete(function(err, responseApi) {
                        if (!err) {
                            console.log('img has been deleted from the cloud in (human-resources): ../controllers/humanResources');
                        } else {
                            console.log("img has NOT been deleted from the cloud in (human-resources): ../controllers/humanResources");
                            console.log(err);
                        }
                    });
                }
                bucket.upload(docs[f].path, {
                    destination: bucket.file(
                        'human-resources/' + humanToChange._id + '/' + key[1] + '.' + fileType[1]
                    )
                }, function(err, file) {
                    if (!err) {
                        console.log('img is now in rocka-bucket... in (human-resources)');
                        humanToChange.name = humanToEdit.human.name;
                        humanToChange.department = humanToEdit.human.department;
                        humanToChange.curp = humanToEdit.human.curp;
                        humanToChange.social_security_number = humanToEdit.human.social_security_number;
                        humanToChange.phone = humanToEdit.human.phone;
                        humanToChange.email = humanToEdit.human.email;
                        humanToChange.birthday = humanToEdit.human.birthday;
                        humanToChange.nationality = humanToEdit.human.nationality;
                        humanToChange.marital_status = humanToEdit.human.marital_status;
                        humanToChange.description = humanToEdit.human.description;
                        humanToChange.address = humanToEdit.human.address;
                        humanToChange.gender = humanToEdit.human.gender;
                        humanToChange.rfc = humanToEdit.human.rfc;
                        humanToChange.location_id = humanToEdit.human.location_id;

                        humanToChange.save(function(err) {
                            if (!err) {
                                console.log('Human Resource has been edited, success: ../controllers/humanResources');
                                res.send(humanToChange);
                            } else {
                                console.log('Human Resource has NOT been edited, fail: ../controllers/humanResources');
                                res.status(400);
                            }
                        });
                        i++;
                    } else {
                        console.log('problem uploading img in rocka-bucket... in: (human-resources)');
                        res.status(400);
                    }
                });
            }
            if (numberOfImgs === i) {
                humanToChange.name = humanToEdit.human.name;
                humanToChange.department = humanToEdit.human.department;
                humanToChange.curp = humanToEdit.human.curp;
                humanToChange.social_security_number = humanToEdit.human.social_security_number;
                humanToChange.phone = humanToEdit.human.phone;
                humanToChange.email = humanToEdit.human.email;
                humanToChange.birthday = humanToEdit.human.birthday;
                humanToChange.nationality = humanToEdit.human.nationality;
                humanToChange.marital_status = humanToEdit.human.marital_status;
                humanToChange.description = humanToEdit.human.description;
                humanToChange.address = humanToEdit.human.address;
                humanToChange.gender = humanToEdit.human.gender;
                humanToChange.rfc = humanToEdit.human.rfc;
                humanToChange.location_id = humanToEdit.human.location_id;

                humanToChange.save(function(err) {
                    if (!err) {
                        console.log('Human Resource has been edited, success in: ../controllers/humanResources');
                        res.send(humanToChange);
                    } else {
                        console.log('Human Resource has NOT been edited, fail in: ../controllers/humanResources');
                        res.status(400);
                    }
                });
            }
        } else {
            console.log('error editing new Human, fail in: ../controllers/humanResources');
            res.status(400);
            console.log('error');
        }
    });
};

exports.deleteHumanResource = function(req, res) {
    var human = req.body.humanResource;
    var descrip = req.body.description;

    var new_human_resource_history = new Human_resources_history();
    new_human_resource_history.description = descrip;
    new_human_resource_history.human_resources_id.push(human._id);

    new_human_resource_history.save(function(err) {
        if (!err) {
            HumanResource.findById(human._id, function(err, humanToGoFromCompany) {
                if (!err) {
                    humanToGoFromCompany.status = human.status;
                    humanToGoFromCompany.save(function(err) {
                        if (!err) {
                            res.send(humanToGoFromCompany);
                        } else {
                            console.log('error deleting human resource in: ../controllers/humanResources');
                            res.status(400);
                        }
                    });
                }
            });
        } else {
            console.log('error finding the human to delete in: ../controllers/humanResources');
            res.status(404);
        }
    });
};