'use strict';

var mongoose = require('mongoose');
var passport = require('passport'), LocalStrategy = require('passport-local').Strategy;

var Users = mongoose.model('User');
var Notification = mongoose.model('Notification');

module.exports = function() {
    passport.use(new LocalStrategy(
        function(username, password, done) {
            console.log('USER');
            console.log(username);
            console.log(password);
            
            Users.findOne({
                email: username
            }).populate('notifications').exec(function(err, user) {
                console.log('USUARIO SERVER');
                console.log(user);
                
                if (user && user.authenticate(password)) {
                    return done(null, user);
                } else {
                    return done(null, false);
                }
            });
        }
    ));

    passport.serializeUser(function(user, done) {
        console.log('serializeUser');
        console.log(user);
        if (user) {
            done(null, user._id);
        }
    });

    passport.deserializeUser(function(id, done) {
        Users.findOne({ _id: id })
        .exec(function(err, user) {
            if (user) {
                return done(null, user);                
            } else {
                return done(null, false);
            }
        });
    });
};