'use strict';
var express = require('express');

module.exports = function(app, routes, users){

	app.get('/partials/*', function(req, res){
	res.render('../public/app/' + req.params[0]);
	});
	//app.use('/bower_components',  express.static(__dirname + '../../bower_components'));
	//CONF FOR HECTOR
	app.use('/bower_components',  express.static('bower_components'));
	app.use('/', routes);
	//app.use('/users', users);

};
