'use strict';

var express     = require('express');
var path        = require('path');
var favicon     = require('serve-favicon');
var logger      = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var passport = require('passport');

module.exports = function(app, config) {
    // view engine setup
    app.set('views', path.join(config.rootPath, 'views'));
    app.set('view engine', 'ejs');

    // uncomment after placing your favicon in /public
    app.use(favicon(path.join(config.rootPath, 'public', 'favicon.ico')));
    app.use(logger('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(session({
        secret: 'roca secret key',
        resave: false,
        saveUninitialized: false
    }));
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(cookieParser());
    app.use(express.static(path.join(config.rootPath, 'public')));
    app.disable('etag');
};