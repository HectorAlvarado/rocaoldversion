'use strict';

var mongoose = require('mongoose');
var passport = require('passport');
var Users = mongoose.model('User');

exports.authenticate = function(req, res, next) {
    var auth = passport.authenticate('local', function(err, user) {
        if (err) {
            return next(err);
        }
        if (!user) {
            res.send({ success: false });
        }
        req.logIn(user, function(err) {
            if (err) {
                return next(err);
            }
            req.session.loged = true;
            req.session.email = req.body.username;
            req.session.password = req.body.password;
            req.session.userInfo = user;

            res.send({ success: true, user: user });
        });
    });
    auth(req, res, next);
};

exports.isAuthenticated = function(req, res, next) {
    if (req.isAuthenticated())
        return next();
    // IF A USER ISN'T LOGGED IN, THEN REDIRECT THEM TO LOGN
    res.redirect('/');
};

exports.userStatus = function(req, res, next) {
    if (!req.isAuthenticated()) {
        return res.status(200).json({
            status: false
        });
    }
    Users.findOne({ email: req.session.email })
        .populate('notifications').exec(function(err, user) {
            //req.session.permissions = JSON.parse(user.role);
            res.status(200).json({
                status: true,
                userInfo: user
            });
        });
};

exports.userLogout = function(req, res, next) {
    req.logout();
    res.status(200).json({
        status: 'Hasta pronto!'
    });
};

exports.permissions = function(req, res, next) {
    if (req.isAuthenticated()) {
        var permissions_received = req.body.permissions;
        if (req.session.permissions === true) {
            return next();
        } else {
            if (req.session.permissions[permissions_received[0]] === true) {

            }
        }
        if (req.session.permissions.hasOwnProperty(permissions_received[0]) && req.session.permissions.hasOwnProperty(permissions_received[1]) && req.session.permissions.hasOwnProperty(permissions_received[2])) {
            console.log('LOS PERMISOS RECIBIDOS');
            console.log(permissions_received);
            return next();
        } else {
            console.log('LOS PERMISOS RECIBIDOS FALSE');
            console.log(permissions_received);
            return res.send(false);
        }
    } else {
        res.redirect('/');
    }
};